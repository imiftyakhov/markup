﻿/**
 * $Id: crypto-ru.js 138502 2008-12-24 15:02:40Z ykalemi $
 *
 * Localised properties. RU.
 *
 */

var CryptoLocal = {};

CryptoLocal.VerifyProcessInAction = "Пожалуйста, подождите - идет верификация подписей...";

CryptoLocal.SignedDataComplete = "Подписание прошло успешно";
CryptoLocal.SignVerifyValid = "Подпись верифицирована";
CryptoLocal.SignVerifyNotValid = "Подпись не верифицирована";

//Label
CryptoLocal.SignatureOwner = "Владелец&nbsp;подписи";
CryptoLocal.SignatureTime = "Дата&nbsp;подписания";
CryptoLocal.SignatureComment = "Комментарий";
CryptoLocal.SignatureStatus = "Состояние&nbsp;проверки";

//Erorr
CryptoLocal.SimpleError = "Извините, произошла ошибка!";
CryptoLocal.CertificateError = "Тип данного сертификата не поддерживается или недоступен центр сертификации";
CryptoLocal.OpenFileCertificateError = "Ошибка открытия файла сертификата";
CryptoLocal.ErrorPathNotSpecified = "Путь до файла с сертификатом не задан";
CryptoLocal.CspNotAllowed = "Тип шифрования данного сертификата не допустим";
CryptoLocal.DigitalSignIsNotEnabled = "Тип сертификата не предназначен для подписания документа";
CryptoLocal.OpenStoreError = "Ошибка открытия хранилища сертификатов";
CryptoLocal.CertificateNotFound = "Сертификат не найден";

CryptoLocal.ErrorSaveCertificate = "Ошибка создания файла сертификата. Для создания файла используйте консоль сертификатов";

CryptoLocal.CERTIFICATE_ISSUED = "Сертификат выдан и установлен успешно";

CryptoLocal.HRESULT_ERROR_CANCELLED = "Действие отменено пользователем";

//Error HRESULT common
CryptoLocal.E_NOTIMPL = "Действие не применимо";
CryptoLocal.E_ABORT = "Операция оборвалась";
CryptoLocal.E_ACCESSDENIED = "Центр сертификации не доступен";
CryptoLocal.E_FAIL = "Произошла неизвестная ошибка";
CryptoLocal.E_HANDLE = "Обработка прошла некорректно";
CryptoLocal.E_INVALIDARG = "Некорректно задан один или более аргументов";
CryptoLocal.E_NOINTERFACE = "Нет поддерживаемого интерфейса";
CryptoLocal.E_OUTOFMEMORY = "Ошибка выделения памяти";
CryptoLocal.E_POINTER = "Данная ссылка не действительна";
CryptoLocal.E_UNEXPECTED = "Неожиданое завершение операции";

//Request disposition
CryptoLocal.CR_DISP_DENIED = "Запрос не принят";
CryptoLocal.CR_DISP_ERROR = "Запрос обработан с ошибкой";
CryptoLocal.CR_DISP_INCOMPLETE = "Запрос не полный";
CryptoLocal.CR_DISP_ISSUED = "Сертификат выдан";
CryptoLocal.CR_DISP_ISSUED_OUT_OF_BAND = "Сертификат выдан";
CryptoLocal.CR_DISP_UNDER_SUBMISSION = "Запрос ожидает подтверждения";

//Error Chain Status
CryptoLocal.CAPICOM_TRUST_IS_NOT_TIME_VALID = "Вы не можете осуществить подпись документа, так как срок действия сертификата истек";
CryptoLocal.CAPICOM_TRUST_IS_NOT_TIME_NESTED = "Вы не можете осуществить подпись документа, так как срок действия сертификата еще не наступил";
CryptoLocal.CAPICOM_TRUST_IS_REVOKED = "Вы не можете осуществить подпись документа, так как сертификат отозван";
CryptoLocal.STATUS_NOT_VALID = "Невозможно определить статус сертификата или он недействителен";

//utils
//may be Date object extend?
CryptoLocal.formatDate = function (date)
{
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var minutes = date.getMinutes();
    var hours = date.getHours();

    if(day < 10) day = "0" + day;
    if(month < 10) month = "0" + month;
    if(minutes < 10) minutes = "0" + minutes;
    if(hours < 10) hours = "0" + hours;

    return [day, month, date.getFullYear()].join(".") + " " + [hours, minutes].join(":");
};
