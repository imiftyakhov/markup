(function ($) {
	$.notifySaveAll = function (options) {
		var defaults = {
			target: $('#SaveAll'),
			container: $('.filling-indicator-wrapper'),
			messageText: 'сохранено успешно',
			errorLevel: 'success'
		}

		var o = $.extend({}, defaults, options);

		var message = $("<span class='" + o.errorLevel + "'/>")
			.text(o.messageText)
			.css({'display': 'none'});

		message.appendTo(o.container);

		var target = o.target;
		target.fadeOut(1000, function () {
			message.fadeIn(1000, function () {
				message.fadeOut(1000, function () {
					message.remove();
					target.fadeIn(1000, function () {
						target.removeClass('notification-activate');
					});
				});
			});
		});
	}

	$.notifyAutoSave = function (options) {
		var defaults = {
			target: $('.autosave-msg')
		}

		var o = $.extend({}, defaults, options);
		var target = o.target;

		var handler = function() {
			this.off( 'click', handler );
			this.fadeOut(1000);
		};

		target.fadeIn(1000);
		target.on("click", handler.bind(target));
	}
}(jQuery));
