/**
 * Created by azimka on 07.07.16.
 */
'use strict';

function initScrollTabsFunction() {	
	if( $(".DemandPage .tab-row, .LotPage .tab-row").length == 0)
		return false;
	
    handleScrollTabEvent();
    handleResizeTabEvent();

    $(window).on({
    	'scroll': handleScrollTabEvent,
    	'resize': handleResizeTabEvent
    });
};

function handleScrollTabEvent() {
    var $tabs = $(".tab-row");
    var $tabPanel = $(".tab-panel");
    
    var scrollY = window.scrollY || document.documentElement.scrollTop;
    var scrollX = window.scrollX || document.documentElement.scrollLeft;    
    var offsetTop = $tabPanel.parent().offset().top;

    var rule = {};
    if (scrollY < offsetTop) {
        rule = $.extend( rule, {position: "absolute", left: '0px' });
    } 
    
    if (scrollY >= offsetTop) {
    	rule = $.extend( rule, {position: "fixed", top: '0px' });
    }

    if ( rule["position"] != "absolute") {
    	var offsetLeft = $tabPanel.offset().left - $tabs.innerWidth() + 1;
        rule = $.extend( rule, {left: (offsetLeft - scrollX) });
    }
    else
    {
    	var positionLeft = $tabPanel.position().left - $tabs.innerWidth();
    	rule = $.extend( rule, {left: positionLeft });
    }

    // update height
    var windowHeight = Math.max( window.innerHeight, $(window).height() );    
    var footerHeight = $(".save-all-form-wrapper").height();
    
    var top = Math.max( 0, $tabPanel.offset().top - scrollY );
    var bottom = Math.min( windowHeight - footerHeight,     		
    		$tabPanel.offset().top + $tabPanel.innerHeight() - scrollY ); //$("#footer").offset().top - scrollY );     
    
    var calculatedWindowHeight = bottom - top;
    
    rule = $.extend( rule, { height: calculatedWindowHeight, maxHeight: calculatedWindowHeight});

//    console.log( $tabPanel.offset() )
//    console.log( $tabs.offset() )
    
    //console.log( rule );
    $tabs.css( rule );
}

function handleResizeTabEvent() {
    //var offset = $("#content").offset();

    var scrollY = window.scrollY || document.documentElement.scrollTop;
    var scrollX = window.scrollX || document.documentElement.scrollLeft;    

    var $tabs = $(".tab-row")
    var $tabPanel = $(".tab-panel");   
    
    var windowHeight = Math.max( window.innerHeight, $(window).height() );    
    var footerHeight = $(".save-all-form-wrapper").height();    

    var top = Math.max( 0, $tabPanel.offset().top - scrollY );
    var bottom = Math.min( windowHeight - footerHeight,     		
    	$tabPanel.offset().top + $tabPanel.innerHeight() - scrollY ); //$("#footer").offset().top - scrollY );     
    
    var calculatedWindowHeight = bottom - top;
    
    var rule = { height: calculatedWindowHeight, maxHeight: calculatedWindowHeight};
    if( $tabs.css("position") != "absolute" )
    {
    	var offsetLeft = $tabPanel.offset().left - $("ul.tabs").innerWidth() + 1;//$tabs.parent().offset();
    	rule = $.extend( rule, {left: (offsetLeft - scrollX) });
    }
    else
   	{
        var positionLeft = $tabPanel.position().left - $("ul.tabs").innerWidth();
    	rule = $.extend( rule, {left: positionLeft });
   	}
    
    //console.log( rule );
    $tabs.css( rule );
};

