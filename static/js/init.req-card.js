/*$(document).ready(function()
{
  //$('#my-modal').modal('show');
  $('.modal').modal({backdrop: true, keyboard: true});
  $("#notifier3").notify({});
  // set resize handler
  $(window).bind("resize", resizeWindow);
  function resizeWindow( e ) {
      updateScrollBox();
  }

  //updateDocument();

  $("a[rel='tooltip']").twipsy();
  if ( typeof( Wicket ) != "undefined" && typeof( Wicket.Window ) != "undefined")
      Wicket.Window.unloadConfirmation = false;
  // init alerts closing
  $(".alert-message").alert()
});*/

//$(window).on('load resize', function()
//{
//	var height = Math.max($(document).height(), $(window).height());
//	if( height > screen.height - 114 )
//		$('.contact-info').css('bottom', 'auto');
//});

$(document).ready(function(){
		//$("a[rel='tooltip']").twipsy();
		$(".mark-column li span").tooltip();
});

function openFilter( target )
{
	var $target = $(target);
	var $underline = $(target).closest('th').find('.underline');
	var container = $( $target.attr('data-target'));

	if( $target.is(".active") )
	{
		$underline.removeClass('active');
		$target.removeClass('active');
	}
	else
	{
	    hideOtherModal( container );
	    adjustModal( container );

	    // hide css
	    $target.closest('table').find('.glyphicon-filter, .underline').removeClass('active');
	    $target.addClass('active');
	    $underline.addClass('active');

	    // load component markup
	    //auxiliaryInits.load( container );
	}
}

function updateDocument()
{
	updateSubtitleScale();
	//applyTooltips();
	updateScrollBox();
}

var datepickers = [];
var datetimepickers = [];

function updatePager()
{
	// setup margin
	var arr =  $(".pager > div > *[id^='page']")
	for( var i = 0; i < arr.length; i = i + 1 )
	{
		var link = $( arr[i] )
		var newMargin = (34 - link.width()) / 2
		link.css({
			'margin-left': newMargin + 'px',
			'margin-right': newMargin + 'px'
		});
	}
}

function updateFooter( state )
{
	var $target = $("#footer");
	$target.removeClass();
	if( state )
		$target.addClass('save-all-active');
}

//recalculate action and fixed column in tables
//$(window).bind('resize', updateTable);

function updateTable()
{
	// fix some column
	$(".fixed-colum-active:visible").each(function( idx, src ) {
		var $table = $(src);
		var container = $table.closest(".scroll-grid-box");
		var cell_map = [];

		$table.closest('.table-container').find('.fixed-column-wrapper').each( function( idx, item ) {
			var $src = $( item );

			var colidx = parseInt( $src.attr('data-target') );
			var $header = $table.find("thead th:nth-child(" + (colidx + 1) + ")");

			var maxWidth = container.innerWidth();
			var cwidth = $header.outerWidth() + 1;
			if( $header.position().left + cwidth > maxWidth)
				$src.css({right: '0px'});
			else if( $header.position().left < 0 )
				$src.css({left: '0px'});
			else
				$src.css({ left: $header.position().left + 'px'});

			$src.css({ top: $table.position().top + 'px', width: cwidth + 'px'});

			// update content
			$table.find("tr").each( function( rowidx, row ) {
				var $row = $(row);
				var $dest_row = $( $src.children()[rowidx] );

				cell_map.push( {
					src: $row,
					dst: $dest_row,
					h: $row.outerHeight() + 2 } );
			});
		})

		$(cell_map).each( function( idx, item ) {
			item.dst.css('height', item.h );
			item.src.css('height', item.h );
		})

		//setTimeout( function() { console.log( JSON.stringify( cell_map )); }, 0);
	});

	// table action column
	$(".table-action-row-wrapper:visible ").each(function( idx, srcContainer ){
		var $srcContainer = $(srcContainer);
		var $cell = $srcContainer.parent()

		if( $cell.innerHeight() < $srcContainer.outerHeight())
		{
			// height + cell-padding
			$cell.css( 'height', $srcContainer.outerHeight() + 10 );
		}
	})
}

function updateActionColumn()
{
	setTimeout( function() {
		$(".action-button-wrapper").html("");

		// fix some column
		$(".scroll-grid-box table:visible").each(function( idx, src ) {
			var $src = $(src);

			if( $src.is('.fixed-colum-active'))
				return true;

			// add marker class
			$src.addClass('fixed-colum-active');

			var container = $src.closest(".scroll-grid-box");
			if( $src.closest( ".table-container" ).length != 0 || container.length == 0 )
				return true; // skip and continue

			var rootDiv = $("<div class='table-container'>").css({
				position: 'relative', width: '100%', height: '100%',
				padding: '0px' });

			container.wrap( rootDiv );

			var maxWidth = container.innerWidth();
			var cols = $src.find("thead tr:last-child").children();
			cols.each(function( idx, header){
				var $header = $(header);

				if( !$header.is(".fixed-column") )
					return;

				var new_table = $("<div class='fixed-column-wrapper' data-target='" + idx + "'>");
				new_table.css({ position: 'absolute',
					height: src.innerHeight + 'px',
					backgroundColor: 'white' });

				// copy content
				$src.find("tr").each( function( rowidx, row ) {
					var $row = $(row);
					var src_cell = $row.children()[idx];
					var $src_cell = $(src_cell);

					var inner_html = $src_cell.clone().html()
					var new_cell = $("<div class='cell'>")
						.append( !inner_html.trim() ? "&nbsp;" : inner_html )
						.addClass( $src_cell.attr('class') )
						.css({ backgroundColor: $row.css('background-color'), width: '100%' });

					var new_row = $("<div class='row'>)")
						.append( new_cell );

					new_table.append( new_row );
				});

				container.parent().append( new_table );
			});
		});

		// init resize
		initResize();

		updateTable();
	}, 0);
}

function hideInlineBlock() {
	showSaveAll();

	$('.inline-block-wrapper:visible').each( function( idx, inlineBlock ){
		var $inlineBlock = $( inlineBlock );
		$inlineBlock
			.attr('id', 'old')
			.find('*').each( function( idx, item){
				$(item).attr('id', '');
			})

		// show button
		showResizeButton( $inlineBlock.closest('.resize') );

		var checkParent = $inlineBlock.closest('tr, .add-inline-toolbar');
		if( checkParent.is('tr') )
		{
			var $table = checkParent.closest('table');
			var scrollBox = checkParent.closest('.scrollbox-y');
		    var scrollX = scrollBox.scrollLeft();
		    var formOffset = scrollBox.is('.active') ? ( $table.outerWidth() - $inlineBlock.outerWidth() ) / 2 : 0;

			$inlineBlock
				.removeAttr('style')
				.css({ position: 'relative', left: formOffset + scrollX });

			var $td = checkParent.find('td');
			$td.css({ height: 'auto'});
		}

		$inlineBlock.slideUp("slow", function() {
			if( checkParent.is('.add-inline-toolbar') )
			{
				$inlineBlock.hide();
				$inlineBlock.remove();

				checkParent.removeClass('active');
				checkParent.children().show();

				var $parent = checkParent.parent();
				if( $parent.is('.border-container') )
				{
					$parent.removeClass('active');
				}
			}
			else
			{
				var old_target = $( '#' + checkParent.attr( 'action-key' ) )
				old_target.removeClass('active');
				old_target.parent().removeClass('active');

				checkParent.remove();
			}
		});
	});
}

function showInlineForm( component, target ) {
	var $component = $(component);

	var $target = $(target);
	var $container = $target.parent();
	var $inlineComponent = $component.find(".inline-block-wrapper");

	var done = function() {
	};

	hideInlineBlock();
	if( !$target.is('.active') )
	{
		// use screen middle
		var documentWidth = $( document ).width();

		var scrollBox = $target.closest('.scrollbox-y');
		var scrollX = scrollBox.scrollLeft();

		if( $container.is('.table-action-row-wrapper') )
		{
			$target.addClass( 'active' );
			$container.addClass( 'active' );

			var table = $target.closest('table').get(0);
			var row = $target.closest('tr').get(0);
			var key = $target.attr('id');

			var table_row = $("<td colspan='" + table.rows[0].cells.length + "' class='edit-row-wrapper'/>")
				.css( { padding: '0px' } );

			var new_row_wrapper = table.insertRow( row.rowIndex + 1 )
			var $new_row_wrapper = $( new_row_wrapper );

			$new_row_wrapper.attr( 'action-key', key);
			$new_row_wrapper.append( table_row );
			$container = table_row;

			var formOffset = scrollBox.is('.active') ? ( $(table).outerWidth() - $inlineComponent.outerWidth() ) / 2 : 0;

		    $inlineComponent.css({ position: 'relative', left: formOffset + scrollX });

			done = function() {
				var $item = $inlineComponent;
				var itemPosition = $item.position();

				$new_row_wrapper.css({ height: $new_row_wrapper.outerHeight() });
				$item.css({ position: 'absolute', left: itemPosition.left, top: itemPosition.top, marginLeft: '0px' });
			};
		}
		else
		{
			$container = $target.closest(".add-inline-toolbar");
			$container.children().hide();
			$container.addClass('active');

			var $parent = $container.parent();
			if( $parent.is('.border-container') )
			{
				$parent.addClass('active');

				var formOffset = scrollBox.is('.active') ? ( $container.outerWidth() - $inlineComponent.outerWidth() ) / 2 : 0;

				$inlineComponent.css({ marginLeft: formOffset + scrollX });
			}
		}

		$inlineComponent.hide();
		var selectWidth = $inlineComponent.innerWidth();

		// init select
		auxiliaryInits.load( $inlineComponent );
//		setTimeout( function() {
//			$inlineComponent.find( 'select' ).chosen({no_results_text: "Не найдено совпадений с", display_selected_options: false});
//		}, 0);

		$container.append( $inlineComponent );
		$inlineComponent.slideDown("fast", done);

		hideSaveAll();

		hideResizeButton( $target.closest('.resize') );
	}
}

function hideSaveAll() {
	// hide save all panel
	$('.save-all-form-wrapper').fadeOut('fast', function() {
		updateFooter( false );
	});
}

function showSaveAll() {
	$('.save-all-form-wrapper').fadeIn('fast', function() {
		updateFooter( true );
	});
}

function hideResizeButton( block ) {
	$(block).find('i.table-rollup-icon, i.table-rollout-icon' ).fadeOut('fast');
}

function showResizeButton( block ) {
	$(block).find('i.table-rollup-icon, i.table-rollout-icon' ).fadeIn('fast', function() {  $(this).removeAttr('style'); });
}

function hideInlineForm(component) {
	var $component = $(component);

	var parent = $component.parent();
	if( parent.is('.table-action-row-wrapper') )
	{
		var table = $component.parents('table');
		var key = parent.attr('id');

		var old_component = table.find('#' + key + '_row');
		old_component
			.attr('id', key)
			.find('*').each(function(idx,item) {

			var $item = $(item);
			$item.attr( 'id', $item.old_id );
		})

		$component.parents('tr').remove();
	}
	else
	{
		$component.remove();
	}
}

function addDatepickerTo(id)
{
    datepickers[id] =
            new DatePicker('#' + id,
                    {
                        pickerClass: 'datepicker_vista',
                        format: 'd.m.Y',
                        inputOutputFormat: 'd.m.Y',
                        allowEmpty:true,
                        toggleElements: '#' + id + '-trigger',
                        days: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                        months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
			            onSelect: function(date){
			            	id && $('#' + id) ? $('#' + id).change() : "";
			            },
			            onKeyup: function(item)
			            {
			            	if( !id || !$('#' + id))
			            		return;

			            	var dateItem = $('#' + id);
			            	var timeoutId = dateItem.data('timeoutid');
			            	//console.info( timeoutId );

			            	if( timeoutId != null )
			            	{
			            		clearTimeout(timeoutId);
			            		dateItem.data('timeoutid', null);
			            		//console.info( "Clear timeout" );
			            	}

			            	//console.info("values: " + item.get('value') + ":" + dateItem.val() );
			            	if(!isDate( item.get('value') ) || ( dateItem.val() == item.get('value') && timeoutId == null ))
			            	{
			            		//console.info( "Not a date ");
			            		return;
			            	}

			            	//console.info("copy value");
            				dateItem.val( item.get('value') );
		            		timeoutId = setTimeout(
		            			function() {
		            				//console.info( "Ready for update" );
		            				dateItem.change();
		            			}, 1000);

		            		//console.info("Set timeoutid: " + timeoutId);
		            		dateItem.data('timeoutid', timeoutId);
			            }
                    });
}

function setDateTimePicker(id)
{
    datetimepickers[id] =
            new DatePicker('#' + id,
                {
                    pickerClass: 'datepicker_vista',
                    timePicker: true,
                    format: 'd.m.Y H:i',
                    allowEmpty:true,
                    inputOutputFormat: 'd.m.Y H:i',
                    toggleElements: '#' + id + '-trigger',
                    days: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                    months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
                });
}

function initConfirms()
{
	$(document).ready(function()
	{
		$.refreshDefValues();
	    setConfirms();

		initSaveOnUserInactive();
        disableSaveAllButton();
	});

    $(document).ajaxComplete(function()
    {
        setTimeout(function(){
        	$.refreshDefValues();
        }, 3000);

        updateHeadersIcons();

    });
}

function setConfirmsIfChanged()
{
	$.refreshDefValues();
    setConfirms();
}

var timeout = null;
function initSaveOnUserInactive()
{
	var inactiveInterval = 10 * 60 * 1000;
	var checkTimeout = 30000;

	setInterval( function callback() {
		if( !timeout && $.hasFormsAnyChanges($("form")))
		{
			timeout = new Date()
		}

		var now = new Date()
		if( timeout && now - timeout > inactiveInterval ) {

			if( !$.hasFormsAnyChanges($("form")) )
			{
				timeout = null
				return true;
			}

			var valueWasSave = false;
    		$("form").map( function( idx, form ) {
    			// save if true
				var check = false;
				try
				{
					check |= $.checkForm( form )
				}
				catch( err ){

				}

    			if( check ) {
	    			try
					{
						$("#" + form.id + " input.btn[type=button][id^='submit']" ).click();
					}
					catch(err)
					{

					}
					valueWasSave = true;
    			}
    		});

    		if( valueWasSave )
			{
				$.notifySave({messageText: 'Сохранено автоматически', errorLevel: 'autosave'});
			}

    	    refreshDefValues();
    		timeout = null;

    		console.log('Попытка сохранения');
		}
	}, checkTimeout);
}

function setConfirms()
{
	$("input.btn[type=button][id^='submit']" ).each( function() {
        $(this).click(function() {
        	timeout = null;
        });
	});
}

var formsToUpdate = [];
function disableSaveAllButton()
{
    setInterval( function callback() {
        var disable = true;
        $("form").map( function( idx, form ) {
    		var $form = $(form);

            var saveButton = $form.find("input.btn[type=button][id^='submit']");
            if( !form.getAttribute('ignoreChanges') && saveButton.length)
            {
                if ( $.hasFormsAnyChanges(form))
                {
                    saveButton.removeAttr('disabled');
                    disable = false;
                    $('#SaveAll').removeAttr('disabled');
                    if ($.inArray(form.parentNode.id, formsToUpdate) == -1)
                    {
                        formsToUpdate.push(form.parentNode.id);
                    }
                }
                else
                {
                    saveButton.attr('disabled', 'disabled');
                    if ($.inArray(form.parentNode.id, formsToUpdate) !== -1)
                    {
                        formsToUpdate.splice($.inArray(form.parentNode.id, formsToUpdate), 1);
                    }
                }

                $form.find('#update').attr('value', function() { return formsToUpdate });
            }
        });

        if( disable ) {
        	$('#SaveAll').attr('disabled', 'disabled')
        } else {
        	$('#SaveAll').removeAttr('disabled');
        }
    }, 1000);
}

function hideOtherModal(thisModal) {
    $('.filter-modal' + ':visible').not(thisModal).each( function( idx, modal ){
    	$(modal).modal('hide');
    })
}

function adjustModal(thisModal) {
    var $modalIcon = thisModal.siblings('.glyphicon-filter');

    var $th = thisModal.closest('th');
    $th.css({ position: 'inherit' });

    var $underline = $th.find('.underline');

    var relativeOffset = $th.position();
    var offsetTop = relativeOffset.top + $th.closest('tr').outerHeight() - 3;
    var offsetLeft = relativeOffset.left + $th.outerWidth() - 1 - $underline.outerWidth();
    $underline.css({ bottom: 'inherit', top: offsetTop + 'px', right: 'inherit', left: offsetLeft + 'px'});

	var iconCenterPos = $modalIcon.offset().left - $th.offset().left + $modalIcon.outerWidth() / 2;
    var leftOffset = relativeOffset.left + iconCenterPos - thisModal.outerWidth() / 2 - 11;
    var topOffset = relativeOffset.top + $th.closest('tr').outerHeight();
    thisModal.css({ left: leftOffset + 'px', top: topOffset + 'px' });

    var hideHandler = function() {
        var $th = $this.closest('th');

    	$th.css({ position: 'relative' });
        $th.find('.underline').css({ bottom: '0px', top: 'inherit', right: '0px', left: 'inherit' });
	}
	thisModal
		.off( 'hidden', hideHandler )
		.on({'hidden':  hideHandler});

}

function initShowHideLinks()
{
    createLinks('showAll');
    createLinks('hideAll');

    function createLinks(name){
        $('a#' + name).bind('click', function(){
            window[name]();
        });
    }
}

function applyTooltips()
{
    var ahelp = $('a.help');
    if (ahelp.size() > 0)
        ahelp.easyTooltip();
}


function updateScrollBox()
{
  $$('.scroll-grid-box', '.scroll-box').each( function(item)
	  {
  		$(item).width( $(item).parent('div').width() );
	  });
}


function updateSubtitleScale()
{
    $("#subtitle").dynamicFontSize({lines: 2, tries: 10, limitWidth: false, squeezeFactor: 0.03});
}

$.fn.rowspan = function(colIdx) {
	var headerCol = $('tbody tr th',this).length;

	return this.each(function() {
		var that;
		var cols = [];
		for(var i=0;i<=colIdx;i=i+1)
		{
			var j = 0;
			$('tr', this).each(function(row) {
				var item = ( headerCol > i ) ?
					$('th:eq(' + i + ')', this) :
					$('td:eq(' + (i - headerCol) + ')', this);

				if( i < colIdx )
					item.filter(':visible').each(function(col) {
						var rowspan = $(this).attr("rowSpan");
						if (rowspan == undefined)
							rowspan = 1;

						for( var k = 0; k < Number(rowspan); k++,j++)
							cols[j] = (!cols[j]?"":cols[j]) + $(this).text();
					});
				else
					item.filter(':visible').each(function(col) {
						cols[j] = (!cols[j]?"":cols[j]) + $(this).text();
						if ( j > 0 && cols[j] == cols[j-1]) {//$(this).text() == $(that).text()) {//if ($(this).text().html() == $(that).html()) {
							var rowspan = $(that).attr("rowSpan");
							if (rowspan == undefined) {
								$(that).attr("rowSpan", 1);
								rowspan = $(that).attr("rowSpan");
							}
							rowspan = Number(rowspan) + 1;
							$(that).attr("rowSpan", rowspan); // do your action for the colspan cell here
							$(this).hide(); // .remove(); // do your action for the old cell here
						} else {
						  that = this;
						}
						that = (that == null) ? this : that; // set the that if not already set
						j = j + 1;
					});
			});
		}
	});
};

$.fn.colspan = function(rowIdx) {
	return this.each(function() {
		var that;
		$('tr', this).filter(":eq(" + rowIdx + ")").each(function(row) {
			$(this).find('th,td').filter(':visible').each(function(col) {
				if ($(this).html() == $(that).html()) {
					var colspan = $(that).attr("colSpan");
					if (colspan == undefined) {
						$(that).attr("colSpan", 1);
						colspan = $(that).attr("colSpan");
					}
					colspan = Number(colspan) + 1;
					$(that).attr("colSpan", colspan);
					$(this).hide(); // .remove();
				} else {
					that = this;
				}
				that = (that == null) ? this : that; // set the that if not
														// already set
			});
		});

	});
};

function isDate(txtDate)
{
	  var currVal = txtDate;
	  if(currVal == '')
	    return true;

	  //Declare Regex
	  var rxDatePattern = /^(\d{1,2})(\/|-|\.)(\d{1,2})(\/|-|\.)(\d{4})$/;
	  var dtArray = currVal.match(rxDatePattern); // is format OK?

	  if (dtArray == null)
	     return false;

	  //Checks for dd/mm/yyyy format.
	  var dtDay= dtArray[1];
	  var dtMonth = dtArray[3];
	  var dtYear = dtArray[5];

	  if (dtMonth < 1 || dtMonth > 12)
	      return false;
	  else if (dtDay < 1 || dtDay> 31)
	      return false;
	  else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
	      return false;
	  else if (dtMonth == 2)
	  {
	     var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
	     if (dtDay> 29 || (dtDay ==29 && !isleap))
	        return false;
	  }
	  else if (dtYear > 2050 || dtYear < 2000)
		  return false;

	  return true;
}

function panelFunction()
{
	var maxTableWidth = 0;
    $('.tab-data table').each(function() {
    	if($(this).width() > maxTableWidth) maxTableWidth = $(this).width();
    });
    var widthBody = $('body').width()-200;
    if (maxTableWidth > widthBody)
    {
    	$('.tab-data > .box').width(maxTableWidth);
    }
    else
    {
        $('.tab-data > .box').css('width', 'auto');
    }
}

$.fn.accordion = function() {
	$(this).addClass("ui-accordion ui-widget ui-helper-reset");
	var headers = $(this).children(".block-header");
	/*
	 * Prepare initial state of blocks. In a first time we show all blocks closed
	 */
	headers.addClass("accordion-header ui-accordion-header ui-helper-reset ui-state-default ui-accordion-icons ui-corner-all");
	headers.each(function () {
		$(this).next().hide();
	});

	headers.append('<span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e">');

	/*
	 * Add onclick handler. If block was closed, open it, adn vice versa.
	 */
	headers.click(function() {
		var header = $(this);
		var panel = $(this).nextAll();

		// :? Rollout table block accordion
		if( panel.is( '.disbaled' ) )
			return;

		// hide action-column-wrapper
		panel.find( $(".action-button-wrapper") ).hide();

		var isOpen = panel.is(":visible");
        isOpen ? hideBlockContent(header, panel) : showBlockContent(header, panel);
	});
};

$.fn.collapsible = function( options )
{
	var o = {};
	var $this = $(this);
	if( $this.is(".ui-collapsible") )
		return;

	$this.addClass("ui-collapsible");

	// default configuration properties
	var defaults = {
		header: $(this).find(".panel-head-wrapper"),
		showButton: null,
		hideButton: null,
		toogleButton: $('<span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e">')
	}

	function panel( button ) {
		var header = $(button).closest('.panel-head-wrapper');
		return header.nextAll();
	}

	var events = {
		hide: function() {
			var target = panel( this );
			target.slideUp("fast", function() {
			        target.hide();
			        if( o.showButton )
			        	o.showButton.removeClass("disable");

			        if( o.hideButton )
			        	o.hideButton.addClass("disable");
			    });
		},

		show: function() {
			var target = panel( this );
			target.slideDown("fast", function() {
			        target.show();
			        if( o.showButton )
			        	o.showButton.addClass("disable");

			        if( o.hideButton )
			        	o.hideButton.removeClass("disable");
			    });
		},

		toogle: function() {
			panel( this ).is(":visible") ?
				o.hide.bint(this)() : o.show.bind(this)();
		}
	}

	$.extend( o, defaults, events, options);
	var $header = $(o.header);

	/*
	 * Prepare initial state of blocks. In a first time we show all blocks closed
	 */
	$header.addClass("accordion-header ui-accordion-header ui-helper-reset ui-state-default ui-accordion-icons ui-corner-all");
	$header.nextAll().hide();

	if( o.showButton || o.hideButton )
	{
		if( $header.find( o.showButton ).length == 0 )
			$header.append( o.showButton );

		if( $header.find( o.hideButton ).length == 0 )
			$header.append( o.hideButton );

		o.showButton.click( o.show );
		o.hideButton.click( o.hide );
	}
	else {
		if( $header.find( o.toogleButton ).length == 0)
			$header.append( o.toogleButton );

		o.toogleButton.click( o.toogle );
	}
};

function hideBlockContent(header, panel){
    panel.slideUp("fast", function() {
        panel.hide();

        header.removeClass("ui-state-active")
            .addClass("ui-state-default")
            .children("span").removeClass("ui-icon-triangle-1-s")
            .addClass("ui-icon-triangle-1-e");

        $(window).trigger( 'resize' );
    });
};

function showBlockContent(header, panel) {
    panel.slideDown("fast", function() {
        panel.show();

        /* refresh action-colum-wrapper */
        updateActionColumn();

        header.removeClass("ui-state-default")
            .addClass("ui-state-active")
            .children("span").removeClass("ui-icon-triangle-1-e")
            .addClass("ui-icon-triangle-1-s");

        $(window).trigger( 'resize' );
    });
};


function hideAll(){
    jQuery(".panelBody>ul>li>div.block-header").each(function() {
        hideBlockContent($(this), $(this).nextAll());
    });
};

function showAll(){
    jQuery(".panelBody>ul>li>div.block-header").each(function(){
        showBlockContent($(this), $(this).nextAll());
    })
};

function updateHeadersIcons(){
    $('.ui-state-default').each(function(){
        if($(this).siblings(':visible').length){
            $(this).removeClass("ui-state-default")
                .addClass("ui-state-active");
        }
    });
}

function hideNavigation()
{
	$("ul.tabs").parent().fadeOut(0);
	$("#scrollUp").animate({opacity:'0'}, 0);
}

function showNavigation()
{
	$("ul.tabs").parent().fadeIn(0);
	$("#scrollUp").animate({opacity:'1'}, 300);
}

function showNavigation()
{
	$("ul.tabs").parent().fadeIn(0);
	$("#scrollUp").animate({opacity:'1'}, 300);
}

(function($) {
    $.fn.hasVScroll = function() {
        return this.get(0) ? parseInt( this.get(0).scrollHeight ) > parseInt( this.innerHeight() ) : false;
    };

    $.fn.hasHScroll = function() {
    	console.log( "sw:" + this.get(0).scrollWidth + "; iw:" + this.innerWidth() );
        return this.get(0) ? parseInt( this.get(0).scrollWidth ) > parseInt( this.innerWidth() ) : false;
    };
})(jQuery);

function initResize() {
	$('.resize').each( function( idx, block ) {

		var $panel = $(block);
		var $table = $panel.find('table');

		var $xybox = $table.closest('.scrollbox');
		var $ybox = $table.closest('.scrollbox-y');

		if( !$ybox.hasHScroll() || $panel.find('.ui-accordion-body-icon').length != 0 )
			return;

		$xybox.addClass("with-scroll");

		var scrollHandler = function() {
			if( $table.outerWidth() < $ybox.outerWidth() )
			{
				$ybox
					.removeClass('with-scroll')
					.find('.table-action-row-wrapper, .table-rollup-icon').removeAttr('style');
			}

			if( $ybox.hasHScroll() )
			{
				$ybox
					.addClass('with-scroll')
					.find('.table-action-row-wrapper, .table-rollup-icon').each( function( idx, button ) {
						var $button = $(button);
						var $ancor = $button.closest('th,td');

						var scrollX = window.scrollX || document.documentElement.scrollLeft;
						var leftOffset = $(window).width() - $ancor.offset().left + $ancor.position().left
								- $button.outerWidth() + scrollX;
						$button.css({ left: leftOffset + 'px' });
					})
			}
		};

		var resizeHandler = function( e ) {
			var scrollX = window.scrollX || document.documentElement.scrollLeft;

			// update width
			$ybox.removeClass('with-scroll')

			var nextLeft = -$panel.offset().left + scrollX;
			if( e )
			{
				$ybox
			    	.width( $( window ).width() )
			    	.css({ left: nextLeft });

				// avoid run cycle
				scrollHandler();
			}
			else
			{
				$ybox
		    		.animate({ left: nextLeft, width: $( window ).width() }, "fast", scrollHandler);
			}
		}

		var unregisterScrollHandler = function() {
			$ybox.removeClass('with-scroll');
			$ybox.off( 'scroll', scrollHandler );
		}

		var zoomBlock = $('<div class="block-resize-table-icon"/>').insertBefore( $xybox );
		var zoomIn = $('<i class="ui-accordion-body-icon table-rollout-icon"/>')
			.appendTo( zoomBlock )
			.on("click", function(){
				if( !$ybox.is(".active") && $('.scrollbox-y.active').length == 0)
				{
					var blockContentHeight = $panel.innerHeight();
					var oldTableHeight = $xybox.innerHeight();

					$xybox
						.nextAll(':not(.block-resize-table-icon)')
						.attr('moved', 'moved')
						.appendTo( $ybox );

					// add marker class
					$panel.addClass('zoom-active');
					$panel.closest('.ui-accordion').find('.ui-accordion-header').addClass('disabled');
					$ybox.addClass("active");

					hideNavigation();
					hideSaveAll();
					$('.table-rollout-icon').addClass('.disabled');

					var toolbarContainer = $('<div class="border-container"/>')
						.css({ width: $table.innerWidth() + 'px' });

					// add footer
					$ybox.find( '.add-inline-toolbar' ).wrap( toolbarContainer );

					$xybox.css({ height: oldTableHeight });
					//$ybox.css({ height: blockContentHeight });

					// scroll event
					$ybox.on('scroll', scrollHandler)

					// register window event
					$(window).on('resize scroll', resizeHandler);

					// invoke resize
					resizeHandler();
				}
			});

		$header = $table.find('th.action-column-wrapper');
		$("<i class=\"ui-accordion-body-icon table-rollup-icon\"/>")
			.appendTo( $header )
			.on("click", function(){
				// add marker class
				$panel.removeClass('zoom-active');
				$panel.closest('.ui-accordion').find('.ui-accordion-header').removeClass('disabled');
				$ybox.removeClass("active");

				showNavigation();
				showSaveAll();
				$('.table-rollout-icon').removeClass('.disabled');

				// unregister window event
				$(window).off('resize scroll', resizeHandler);
				unregisterScrollHandler();

				// move on old place
				$ybox.find('[moved]').removeAttr('moved').insertAfter( $xybox );
				$ybox.find('.border-container').remove();

				$ybox
					.width("auto")
					.css( {left: '0px'});

				$xybox.height("auto");
				//$ybox.height("auto");
			});
	})
}

/**
 * Function creates module and call for init method
 * @param module
 * @returns {*}
 */
function library(module) {
	$(function () {
		if (module.init) {
			module.init();
		}
	});
	return module;
}

/**
 * Добавляем класс, используемый методом startProgress как признак необходимости
 * остановки и удаления progress bar-а.
 */
function addHideClassToProgressBar(){
    //Классом hide помечаем, что элемент требуется убрать
    $(".savingProgressBar").addClass("hide");
}

/**
 * Запуск progress bar-a
 * @param progressBarID
 */
function startProgress(progressBarID) {
    var elem = $("." + progressBarID);
    var width = 0;
    var id = setInterval(iteration, 30);
    function iteration() {
        if ($(elem).hasClass('hide')) {
            clearInterval(id);
            $(elem).parent().remove();
        } else {
            //Здесь, а также в интервале вызова функции можно изменить скорость роста
            width += (98 - width) / 100;
            elem.width (width + '%');
        }
    }
}
/**
 * Обернутый метод addProgressBar для дат с проверкой их изменения.
 *
 * @param inputDate input с датой.
 * @param insertAfterSelector Селектор элемента, к которому прикрепляется progress bar
 * @param progressBarClass Класс для добавления свойств progress bar-у.
 */
function addDateChangedOnlyProgressBar(inputDate, insertAfterSelector, progressBarClass){
    //Если дата не была изменена - progress bar не добавляем
    if($(inputDate).val() == $(inputDate).data("prevValue")) {
        return;
    }
    addProgressBar(insertAfterSelector, progressBarClass);

    //пересохраняем предыдущее значение для проверки изменений
    $(inputDate).data("prevValue", $(inputDate).val());
}

/**
 * Метод добавления progress bar-а.
 * @param insertAfterSelector Селектор элемента, к которому прикрепляется progress bar
 * @param progressBarClass Класс для добавления свойств progress bar-у.
 */
function addProgressBar(insertAfterSelector, progressBarClass) {
    if (!$('.savingProgressBar').length) {
        var $progressBar = $(
                "<div class='pace  pace-active block-status-bar'>" +
                "<div class='savingProgressBar'>" +
                "</div><div class='pace-activity block-progress-bar-wheel'></div>" +
                "</div>");

        $progressBar.insertAfter(insertAfterSelector);
        $(".savingProgressBar").addClass(progressBarClass);
        $(".block-progress-bar-wheel").addClass(progressBarClass);

        //запуск процесса визуализации загрузки.
        startProgress("savingProgressBar");
    }
}

function clickOnSaveButtons() {
    $('#SaveAll').addClass('notification-activate');
    $("input[type='button'][value='Сохранить']").each(function () {
        if (!$(this).closest('.inline-block-body, .modal').length) {
            this.click()
        }
    });
    $(".save-all-form-wrapper a#footerButton")[0].click();
    addProgressBar("div.pace", "fixed-position-bar");
}

function clickOnDownloadButton() {
    $(".save-all-form-wrapper a#footerButton")[0].click()
}

function clickOnElectronicVersionButton() {
    $(".demand-finish-btn").click()
}
/**
 * Функция подготавливает строку в формате <id блока c ошибкой>*<имя блока с ошибкой>*...
 * для дальнейшей обработки $.notifySave()
 * @returns {string}
 */
function selectInvalidBlocks() {
	var blocksWithErrors = $("li:has(input[aria-invalid='true'])");
	var result = '';
	blocksWithErrors.each(function () {
		result += '#' + $(this).attr('id') + '*' + $(this.getElementsByTagName('h4')).text() + '*';
	})
	return result;
}
/**
 * Функция объединяет перечни блоков с ошибками с клиентской валидации
 * и блоков с ошибками серверной валидации
 * @param serverErrors
 * @param clientErrors
 * @returns {Map}
 */
function mergeErrors(serverErrors, clientErrors) {
	var errors = (serverErrors + clientErrors).split('*');
	var elementsIdName = new Map();
	for (var i = 0; i < errors.length - 1; i += 2) {
		elementsIdName.set(errors[i], errors[i + 1]);
	}
	return elementsIdName;
}

/**
 * Переключение по вкладкам
 * @param event
 * @param activeTabName
 */
function selectTab(currentTarget, activeTabName) {
    $(".tabcontent").hide();
    $(".tablinks").parent().removeClass("active");
    $("#" + activeTabName).show();
    $(currentTarget).parent().addClass("active");
}

/**
 * Auxiliary namespace for DemandPage jQuery calls
 * @type {*}
 */
var auxiliaryInits = library(function() {
	return {
		init: function() {},

		load: function( target ) {
			var $target = (target) ? $(target) : $('body');

			setTimeout( function() {
				auxiliaryInits.initCheckBox( target );
				auxiliaryInits.initRadio( target );
				auxiliaryInits.addDatePickerOnField( target );

				$target.find( 'select' ).chosen({no_results_text: "Не найдено совпадений с", display_selected_options: false});
			}, 0);
		},

		initCollapsible: function( target ) {
			$(target).collapsible({
				showButton: $('<a id="' + target.id + '-show">Развернуть</a>'),
				hideButton: $('<a id="' + target.id + '-hide" class="disable">Свернуть</a>')
			});
		},

		addAccordionToTabs: function() {
			$('.tab-panel .panelBody > ul > li').accordion();

			// reinit left panel
			$(window).trigger( 'resize' );
		},

		addDatePickerOnField: function( target ) {
			var $target = (target) ? $(target) : $('body');

			var $datepicker = $target.find(".input-group.date").on({
				show: function() {
					var $item = $(this);

	                $item.datepicker({
	                    format: "dd.mm.yyyy",
	                    maxViewMode: 2,
	                    todayBtn: true,
	                    todayHighlight: true,
	                    clearBtn: true,
	                    autoclose: true,
	                    orientation: 'right bottom',
	                    language: "ru",
	                    overflow: "body",
	                    container: $item.closest('.modal-content').length > 0 ? $item.closest('.modal-content') : 'body',
	                    zIndexOffset: 510
	                }).on('hide', function() {
	                	$item.validate().form();
	                });
				}
			});

			setInterval( function() {
				$datepicker.filter('[_vt=hide]:visible, :not([_vt]):visible')
					.attr('_vt', 'show')
					.trigger("show");
			}, 200);
		},

		initMultiChoise: function( target ) {
			var $target = (target) ? $(target) : $('body');
			var $multiple = $target.find("select[multiple='multiple']").on({
				show: function() {
					$(this).chosen({no_results_text: "Не найдено совпадений с", display_selected_options: false});
				}
			}).css({opacity: 0});

			setInterval( function() {
				$multiple.filter('[_vt=hide]:visible, :not([_vt]):visible')
					.attr('_vt', 'show')
					.css({opacity: 1})
					.trigger("show");
			}, 200);
		},

		initSingleChoise: function( target ) {
			var $target = (target) ? $(target) : $('body');
			var $choice = $target.find("select:not([multiple])").on({
				show: function() {
					$(this).chosen({no_results_text: "Не найдено совпадений с"});
				}
			}).css({opacity: 0});

			setInterval( function() {
				$choice.filter('[_vt=hide]:visible, :not([_vt]):visible')
					.attr('_vt', 'show')
					.css({opacity: 1})
					.trigger("show");
			}, 100);
		},

		initRadio: function( target ) {
			var $target = (target) ? $(target) : $('body');

			var $radiogroup = $target.find(".radiogroup").on({
				show: function() {
					$(this).customRadio();
				}
			});

			$radiogroup.trigger("show");
//			setInterval( function() {
//				$radiogroup.filter('[_vt=hide]:visible, :not([_vt]):visible')
//					.attr('_vt', 'show')
//					.trigger("show");
//			}, 100);

		},

		initCheckBox: function( target ) {
			var $target = (target) ? $(target) : $('body');
			var $checkbox = $target.find("[type=checkbox]").on({
    			click: function() {
    				var $this = $(this);
    				$this.next("span.checkbox_custom").toggleClass('checked', $this.is(':checked'));
    			},
    			focus: function() { $(this).next("span.checkbox_custom").addClass('focus'); },
    			blur: function() { $(this).next("span.checkbox_custom").removeClass('focus'); },
				show: function() { $(this).customCheck(); }
			});

			$checkbox.trigger("show");
//			setInterval( function() {
//				$checkbox.filter('[_vt=hide]:visible, :not([_vt]):visible')
//					.attr('_vt', 'show')
//					.trigger("show");
//			}, 100);
		},

		initScrollUp: function() {
			$.scrollUp({
				scrollName: 'scrollUp', // Element ID
				topDistance: '500', // Distance from top before showing element (px)
				topSpeed: 300, // Speed back to top (ms)
				animation: 'fade', // Fade, slide, none
				animationInSpeed: 200, // Animation in speed (ms)
				animationOutSpeed: 200, // Animation out speed (ms)
				scrollText: 'Наверх', // Text for element
				activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
				useSpan: true, // adds span before text
				spanGlyphClass: 'glyphicon-chevron-up', // add css class to the span
				buttonClass: 'btn secondry',
				container: $('#content')
			});
		},

		initNotifySave: function (errors) {
			if (errors.size == 0) {
				$.notifySave({
					messageText: 'сохранено успешно',
					errorLevel: 'success'
				})
			} else {
				$.notifySave({
					messageText: 'Блоки с ошибками:',
					errorLevel: 'validate-error',
					elementsIdName: errors
				})
			}
		}
	};
}());
