/* $Id: crypto.js 244625 2012-01-13 04:06:29Z aparshakov $
 *
 * Digital signature & Encryption
 *
 */

var SERVLET_URL = '/crypto';

function signDemand(sourceEl, id)
{
	if (!$.browser.msie) {
		alert('Подписывание документов возможно только в браузере Internet Exporer');
		return;
	}
	
	var filesData = null;
	$.ajaxSetup({ async: false });
	try {
		$.get(SERVLET_URL, { action: "get_doc_ids", demand_id: id },
				   function(data) {
				     //alert("Data Loaded: " + JSON.stringify(data/*.evalJSON()*/));
				     filesData = data;
				   }
			);
		$.ajaxSetup({ async: true });
	} catch(e) {
		$.ajaxSetup({ async: true });
	}
	
	if (filesData == null) {
		alert('Ошибка при поиске файлов для подписи');
		return;
	}
	
	if (filesData.files.length == 0) {
		alert('Нет файлов для подписи');
		return;
	}
	
	var form = $(sourceEl).parents('form');
	var infoMsgContainer = $('div[id^="message"]', form);
	infoMsgContainer.html('');
	
	var success = true;
	
	var signer = new SignContent(id);
	signer.getAndCheckCertificate();
	
	if (signer.isError){
		alert('Ошибка при проверке сертификата: ' + signer.error_description);
		return;
	}
	
	for (var i = 0; i < filesData.files.length; i++) {
		var fileInfo = filesData.files[i];

		signer.sign(fileInfo.id);
		if (signer.isError) {
			success = false;
			alert('Не удается подписать файл ' + fileInfo.title + '. Ошибка: ' + signer.error_description);
			break;
		} else {
			infoMsgContainer.html(infoMsgContainer.html() + (infoMsgContainer.innerHTML == '' ? '' : '<br>') + 'Подписан файл ' + fileInfo.title);
		}
	}
	if (success) {
		alert(CryptoLocal.SignedDataComplete);
	}
}

/**
 * Upload "detached signature" of data with "uuid" to server
 * @return uuid of signature object from server
 */
function uploadDetachedSignature(signature, demandId, fileId)
{
    //var utils = new ActiveXObject('CAPICOM.Utilities');
    //var uuidb64 = utils.Base64Encode(uuid);
    //var action = utils.Base64Encode('0'); // detached signature
    var resultId = '';
    
    $.ajaxSetup({ async: false });
	try {
		$.post(SERVLET_URL, { action: "upload_ds", demand_id: demandId, file_id: fileId, ds: signature },
				function(data) {resultId = data;}
			);
		$.ajaxSetup({ async: true });
	} catch(e) {
		$.ajaxSetup({ async: true });
		throw e;
	}
    //utils = null;
    return resultId;
}

/*
 * Upload "attached signature" with "signId" of data to server
 * @return uuid of signature object from server
 
function uploadAttachedSignature(signature, uuid, signId)
{
    var utils = new ActiveXObject('CAPICOM.Utilities');
    var uuidb64 = utils.Base64Encode(uuid);
    var signIdBase64 = utils.Base64Encode(signId);
    var action = utils.Base64Encode('1'); // attached signature
    var resultId = '';
    new Ajax.Request(SERVLET_URL, {
              onSuccess: function(t) {resultId = t.responseText;},
              asynchronous: false,
              postBody: uuidb64 + action + signIdBase64 + signature,
              method: 'POST',
              contentType: 'text/html'
    });
    utils = null;
    return resultId;
}*/

/*
 * Upload time stamp to server
 
function uploadStamp(Stamp, uuid, signId)
{
    var utils = new ActiveXObject('CAPICOM.Utilities');
    var uuidb64 = utils.Base64Encode(uuid);
    var action = utils.Base64Encode('2'); // stamp
    var signIdBase64 = utils.Base64Encode(signId);
    new Ajax.Request(SERVLET_URL, {
              onSuccess: function(t) {},
              asynchronous: false,
              postBody: uuidb64 + action + signIdBase64 + Stamp,
              method: 'POST',
              contentType: 'text/html'
    });
    utils = null;
}*/

function OpenStore(storeName)
{
    var MyStore = new ActiveXObject('CAPICOM.Store');

    var locationStore = CAPICOM.CAPICOM_STORE_LOCATION.CAPICOM_CURRENT_USER_STORE;
    if(!storeName) storeName = CAPICOM.CAPICOM_STORE_NAME.CAPICOM_MY_STORE;
    var openMode = CAPICOM.CAPICOM_STORE_OPEN_MODE.CAPICOM_STORE_OPEN_READ_ONLY;

    //open the current users personal certificate store
    try
    {
        MyStore.Open(locationStore, storeName, openMode);
        return MyStore;
    }
    catch (e)
    {
        if (e.number != CAPICOM.CAPICOM_ERROR_CODE.CAPICOM_E_CANCELLED)
        {
           throw CryptoLocal.OpenStoreError;
        }
    }

    MyStore = null;
}

/**
 * find certificates from store using system wizard
 */
function FilterCertificate(template)
{
    with(CAPICOM.CAPICOM_CERTIFICATE_FIND_TYPE) {

    var CAPICOM_DIGITAL_SIGNATURE_KEY_USAGE = 128;
    var CERT_KEY_SPEC_PROP_ID = 6;

    var MyStore = OpenStore(CAPICOM.CAPICOM_STORE_NAME.CAPICOM_MY_STORE);


    var FilteredCertificates;

    if(template)
    {
        FilteredCertificates =
             MyStore.Certificates
                    .Find(CAPICOM_CERTIFICATE_FIND_KEY_USAGE, CAPICOM_DIGITAL_SIGNATURE_KEY_USAGE)
                    .Find(CAPICOM_CERTIFICATE_FIND_TIME_VALID)
                    .Find(CAPICOM_CERTIFICATE_FIND_EXTENDED_PROPERTY, CERT_KEY_SPEC_PROP_ID)
                    .Find(CAPICOM_CERTIFICATE_FIND_TEMPLATE_NAME, template);
    }
    else
    {
        FilteredCertificates =
             MyStore.Certificates
                    .Find(CAPICOM_CERTIFICATE_FIND_KEY_USAGE, CAPICOM_DIGITAL_SIGNATURE_KEY_USAGE)
                    .Find(CAPICOM_CERTIFICATE_FIND_TIME_VALID)
                    .Find(CAPICOM_CERTIFICATE_FIND_EXTENDED_PROPERTY, CERT_KEY_SPEC_PROP_ID);
    }

    } //end with

    return FilteredCertificates.Select().Item(1);
    // Clean Up
    //FilteredCertificates = null;
}

/**
 * find certificate from store using hash
 * @throws string error
 */
function FindCertificateByHash(hash)
{
    var findType = CAPICOM.CAPICOM_CERTIFICATE_FIND_TYPE.CAPICOM_CERTIFICATE_FIND_SHA1_HASH;
    var MyStore = OpenStore(CAPICOM.CAPICOM_STORE_NAME.CAPICOM_MY_STORE);
    // find all of the certificates that have the specified hash
    var FilteredCertificates = MyStore.Certificates.Find(findType, hash);

    if(FilteredCertificates.Count)
    {
        return FilteredCertificates.Item(1);
    }
    else
    {
        throw CryptoLocal.CertificateNotFound;
    }

    // Clean Up
    MyStore = null;
    FilteredCertificates = null;
}

/**
 * Check status certificate
 */
function checkCertificateStatus(Certificate)
{
    with (CAPICOM.CAPICOM_CHECK_FLAG)
    {

    Certificate.IsValid().CheckFlag = ( CAPICOM_CHECK_TRUSTED_ROOT | CAPICOM_CHECK_TIME_VALIDITY |
            CAPICOM_CHECK_SIGNATURE_VALIDITY | CAPICOM_CHECK_ONLINE_REVOCATION_STATUS);

    }//end with

    if (Certificate.IsValid().Result == true)
    {
        return true;
    }

    var Chain = new ActiveXObject("CAPICOM.Chain");
    Chain.Build(Certificate);

    with(CAPICOM.CAPICOM_CHAIN_STATUS) {

        // Check Time valid chain of certificates!
        if(CAPICOM_TRUST_IS_NOT_TIME_VALID & Chain.Status)
        {
            var date = CryptoLocal.formatDate(new Date(Certificate.ValidToDate));
            return CryptoLocal.CAPICOM_TRUST_IS_NOT_TIME_VALID + " " + date;
        }

        if(CAPICOM_TRUST_IS_NOT_TIME_NESTED & Chain.Status)
        {
            var date = CryptoLocal.formatDate(new Date(Certificate.ValidFromDate));
            return CryptoLocal.CAPICOM_TRUST_IS_NOT_TIME_VALID + " " + date;
        }

        if(CAPICOM_TRUST_IS_REVOKED & Chain.Status)
        {
            return CryptoLocal.CAPICOM_TRUST_IS_REVOKED;
        }

    } //end with

    for(var status in CAPICOM.CAPICOM_CHAIN_STATUS)
    {
        if(CAPICOM.CAPICOM_CHAIN_STATUS[status] & Chain.Status)
        {
            return CryptoLocal.STATUS_NOT_VALID_OR_ERROR + " " + status;
        }
    }

    return CryptoLocal.STATUS_NOT_VALID_OR_ERROR + " code: " + Chain.Status;
}

/**
 * Get certificate thumbprint from certificate by path on local machine
 * @throws Exception
 */
function getHashFromFile(fileFieldName)
{
    var path = document.getElementById(fileFieldName);

    if(path && path.value)
    {
        var fs, data = null, cert;
        var ForReading = 1;
        var certFile;

        fs = new ActiveXObject("Scripting.FileSystemObject");
        try
        {
            data = fs.OpenTextFile(path.value, ForReading, false);
            certFile = data.ReadAll();
        }
        catch(e)
        {
            throw CryptoLocal.OpenFileCertificateError + " : " + e.description;
        }
        finally
        {
            if(data) data.Close();
        }

        try
        {
            cert = new ActiveXObject("CAPICOM.Certificate");
            cert.Import(certFile);
            return cert.Thumbprint;
        }
        catch(e)
        {
            //todo:
            throw CryptoLocal.CertificateError;
        }
        cert = null;
        data = null;
        fs = null;
    }
    else if (path && !path.value)
    {
        throw CryptoLocal.ErrorPathNotSpecified;
    }

    return null;
}

function refresh_page()
{
    window.location.reload(true);
}

/**
 * Class for support verify signatures  of content data with "contentUUID"
 */
/*var VerifyContent = Class.create({

    initialize: function (container, contentUUID, params)
    {
         //default options
        this.useTimeStamp = false;
        this.container = container;
        this.contentUUID = contentUUID;
        this.signatures = null; // list of signatures

        Object.extend(this, params);
    },

    verify: function ()
    {
        $(this.container).childElements().invoke('remove');
        $(this.container).appendChild(document.createElement('span')).innerHTML = CryptoLocal.VerifyProcessInAction;
        this._downloadSignedContent(this.contentUUID);
        this._downloadSignatures();
    },

    _downloadSignedContent: function (uuid)
    {
        if(!uuid) uuid = this.contentUUID;
        var content = null;
        new Ajax.Request(SERVLET_URL, {
                  parameters: {'signobjectuuid': uuid, 'action': 1},
                  onSuccess: function(t)
                      {
                          var utils = new ActiveXObject('CAPICOM.Utilities');
                          content = utils.Base64Decode(t.responseText);
                      },
                  asynchronous: false
                  }
        );
        this.content = content;
    },

    _downloadSignatures: function (uuid)
    {
        if(!uuid) uuid = this.contentUUID;
        new Ajax.Request(SERVLET_URL, {
                  parameters: {signobjectuuid: uuid, action: 'signatures'},
                  onSuccess: this._onSuccess_DownloadSignatures.bind(this),
                  asynchronous: true
        });
    },

    _onSuccess_DownloadSignatures: function ( response )
    {
        this.signatures = response.responseJSON;
        //initialize table
        var table = document.createElement('table');

        $(this.container).appendChild(table);

        table.id = this.container + '_table';
        Element.addClassName(table, 'supp');
        Element.setStyle(table, {width: '100%', border: 0, margin: 0, padding: 0});

        var row = table.insertRow();
        row.appendChild(document.createElement('td')).innerHTML = "<b>" + CryptoLocal.SignatureOwner + "</b>";
        row.appendChild(document.createElement('td')).innerHTML = "<b>" + CryptoLocal.SignatureTime + "</b>";
        row.appendChild(document.createElement('td')).innerHTML = "<b>" + CryptoLocal.SignatureComment + "</b>";
        row.appendChild(document.createElement('td')).innerHTML = "<b>" + CryptoLocal.SignatureStatus + "</b>";
        this._verifyAll();

        // info remove - span element
        $(this.container).firstDescendant().remove();
    },

    //item -> {sign: .., author: .., : comment: .., stamp: ...}
    // @see CryptoSignServlet.listSignaturesJSON
    _verify: function ( item )
    {
        var SignedData = new ActiveXObject('CAPICOM.SignedData');
        SignedData.Content = this.content;
        var Signature = item.sign;
        var utils = new ActiveXObject('CAPICOM.Utilities');
        try
        {
            SignedData.Verify(Signature, true, CAPICOM.CAPICOM_SIGNED_DATA_VERIFY_FLAG.CAPICOM_VERIFY_SIGNATURE_AND_CERTIFICATE);

            var Signer = SignedData.Signers.Item(1);

            //var certStatus = checkCertificateStatus(SignedData.Signers.Item(1));

            var result = Object.extend({}, item);
            var error = false;

            with (CAPICOM.CAPICOM_ATTRIBUTE) {

                var attrs = Signer.AuthenticatedAttributes;
                for(var i = 1; i <= attrs.Count; i++ )
                {
                    var valueItem = attrs.Item(i).Value;
                    switch(attrs.Item(i).Name)
                    {
                        case CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME:
                            result.signDate = CryptoLocal.formatDate(new Date(valueItem));
                            break;
                        case CAPICOM_AUTHENTICATED_ATTRIBUTE_DOCUMENT_DESCRIPTION:
                            result.comment = valueItem;
                            break;
                        case CAPICOM_AUTHENTICATED_ATTRIBUTE_DOCUMENT_NAME:
                            result.docName = valueItem;
                            break;
                        default:
                            //default
                    } //end switch
                } // end for
                attrs = null;
            } //end with

            if (!result.author)
            {
                result.author = Signer.Certificate.GetInfo(CAPICOM.CAPICOM_CERT_INFO_TYPE.CAPICOM_INFO_SUBJECT_SIMPLE_NAME);
            }

            if(this.useTimeStamp)
            {
                var stamp = new ActiveXObject('TSPCOM.TSPStamp');
                stamp.Import(utils.Base64Decode(item.stamp));
                var hd = new ActiveXObject('CAPICOM.HashedData');
                hd.Algorithm = 0;
                hd.Hash(utils.Base64Decode(Signature));
                error = hd.Value != stamp.HashValue;
                stamp = null;
            }

            this._showResult(result, error ? CryptoLocal.SignVerifyNotValid : '');
        }
        catch (e)
        {
            //error
            this._showResult(item, CryptoLocal.SignVerifyNotValid + "<br> " + e.description);
        }

        utils = null;
        SignedData = null;
    },

    _showResult: function(result, errorMsg)
    {
        var table = $(this.container + '_table');
        var row = table.insertRow();

        row.appendChild(document.createElement('td')).innerHTML = result.author;
        if(!errorMsg)
        {
            row.appendChild(document.createElement('td')).innerHTML = result.signDate;
            row.appendChild(document.createElement('td')).innerHTML = result.comment;
            row.appendChild(document.createElement('td')).innerHTML = CryptoLocal.SignVerifyValid;
        }
        else
        {
            row.appendChild(document.createElement('td')).innerHTML = "";
            row.appendChild(document.createElement('td')).innerHTML = "";
            var error = row.appendChild(document.createElement('td'));
            error.innerHTML = errorMsg;
            Element.setStyle(error, {'color':'red'});
        }
    },

    _verifyAll: function ()
    {
        for(var i = 0; i < this.signatures.length ; i++)
        {
            this._verify( this.signatures[i] );
        }
        auto_resize();
    }

});*/

function SignContent(demandId)
{
	this.demandId = demandId;
    this.isError = false;
    this.mode = 1;
    
    this.getAndCheckCertificate = function()
    {
    	try
        {
            this.Signer = new ActiveXObject('CAPICOM.Signer');
            var certificate = FilterCertificate();

            if(certificate)
            {
            	this.Signer.Certificate = certificate;
                certificate = null;
            }
            else
            {
                throw CryptoLocal.CertificateError;
            }

            var validation = this.validateCert(this.Signer.Certificate);
            
            if(validation === false)
            {
            	this.isError = true;
            	this.error_description = CryptoLocal.CertificateNotFound;
            }
            else if (validation != true)
            {
            	this.isError = true;
            	this.error_description = validation;
            }
        }
        catch (e1)
        {
        	this.processError(e1);
        }    	
    };
    
    this.sign = function (fileId)
    {
    	this.fileId = fileId;
        var signatureComment = "";
        /*if(this.commentField)
        {
            signatureComment = $(this.commentField).getValue();
        }*/
        try
        {
            with (CAPICOM.CAPICOM_ATTRIBUTE) {
                var Today = new Date();
                var TimeAttribute = new ActiveXObject('CAPICOM.Attribute');
                TimeAttribute.Name = CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME;
                TimeAttribute.Value = Today.getVarDate();
                Today = null;
                this.Signer.AuthenticatedAttributes.Add(TimeAttribute);

                if(signatureComment) {
                    var CommentAttribute = new ActiveXObject('CAPICOM.Attribute');
                    CommentAttribute.Name = CAPICOM_AUTHENTICATED_ATTRIBUTE_DOCUMENT_DESCRIPTION;
                    CommentAttribute.Value = signatureComment;
                    this.Signer.AuthenticatedAttributes.Add(CommentAttribute);
                    CommentAttribute = null;
                }
            } // end with

            //download signed content
            
            var self = this;
            $.ajaxSetup({ async: false });
        	try {
        		$.get(SERVLET_URL, {demand_id: this.demandId, file_id: this.fileId, action: 'load_content'},
    				function(data) {
						//alert(data);
						self.download_onSuccess(data);
    				}
        		);
        		$.ajaxSetup({ async: true });
        	} catch(e) {
        		$.ajaxSetup({ async: true });
        		throw e;
        	}
        }
        catch (e1)
        {
        	this.processError(e1);
        }
    };
    
    this.validateCert = function (Certificate)
    {
        if(!Certificate) return false;

        if(!Certificate.KeyUsage().IsDigitalSignatureEnabled)
        {
            return CryptoLocal.DigitalSignIsNotEnabled;
        }

        var cert_rk = Certificate.PrivateKey;
        var cert_uk = Certificate.PublicKey();
        var key_length = cert_uk.Length;
        var csp_name = cert_rk.ProviderName;
        var equil_csp = csp_name == CRYPTO_PRO_CSP_NAME;
        var equil_key_length = key_length == KEY_LENGTH;

        if(equil_csp && equil_key_length)
        {
            return checkCertificateStatus(Certificate);
        }

        //see settings in server application
        return CryptoLocal.CspNotAllowed;
    };
    
    this.download_onSuccess = function (data)
    {
        try
        {
            var SignedData = new ActiveXObject('CAPICOM.SignedData');
            var utils = new ActiveXObject('CAPICOM.Utilities');

            SignedData.Content = utils.Base64Decode(data);
            var signature = SignedData.Sign(this.Signer, CAPICOM.CAPICOM_SIGNATURE_TYPE.DETACHED, CAPICOM.CAPICOM_ENCODING_TYPE.CAPICOM_ENCODE_BASE64);
            var signId = uploadDetachedSignature(signature, this.demandId, this.fileId);

            /*if (this.useTimeStamp)
            {
                createTimeStamp(signature, this.uuid, this);
            }*/

            if (this.useAttachedSignature)
            {
                signature = SignedData.Sign(this.Signer, CAPICOM.CAPICOM_SIGNATURE_TYPE.ATTACHED, CAPICOM.CAPICOM_ENCODING_TYPE.CAPICOM_ENCODE_BASE64);
                uploadAttachedSignature(signature, this.fileId, signId);
            }

            SignedData = null;
        }
        catch (e)
        {
        	this.processError(e);
        }
    };
    
    this.processError = function (e1)
    {
    	this.Signer = null;
    	
    	this.error_description = 'Ошибка';
    	
    	this.isError = true;
    	if (e1.description)
    		this.error_description = e1.description;
    	else
    		this.error_description = e1.message;
        
        if (this.error_description.length > 42) {
            if (this.error_description.substr(0, 42) == 'The requested operation has been cancelled')
            {
            	this.error_description = CryptoLocal.HRESULT_ERROR_CANCELLED;
            }
        }
        
        if (this.error_description == 'Невозможно создание объекта сервером программирования объектов') {
        	this.error_description += '. Возможно не подключена надстройка CAPICOM или сайт не внесен в доверенные.';
        }    	
    };
}

/**
 * Sign content object support
 */
/*var SignContent = Class.create({

    initialize: function (container, params)
    {
        // default options
        this.mode = 0;
        this.container = container;
        this.isError = false;
        this.add_param = '';

        Object.extend(this, params);
        if (this.add_param === 'sign_action')
        	this.mode = 1;

        this.onClickObserver = params.sign_onClick.bindAsEventListener($(container), this);
        $(container).observe('click', this.onClickObserver);
    },

   /**
     * Sign data, target is element button wich clicked;
     /
    sign: function( )
    {
        var uuid = this.uuid;
        var signatureComment = "";
        if(this.commentField)
        {
            signatureComment = $(this.commentField).getValue();
        }
        try
        {
            var Signer = new ActiveXObject('CAPICOM.Signer');
            var hash = this.certificateHash;
            var certificate = null;
            if(!hash && this.fileField)
            {
                hash = getHashFromFile(this.fileField);
            }

            if(!hash)
            {
                certificate = FilterCertificate();
            }

            if(certificate)
            {
                Signer.Certificate = certificate;
                certificate = null;
            }
            else if (hash)
            {
                Signer.Certificate = FindCertificateByHash(hash);
            }
            else
            {
                throw CryptoLocal.CertificateError;
            }

            var validation = this.validateCert(Signer.Certificate);

            if(validation === true)
            {
                with (CAPICOM.CAPICOM_ATTRIBUTE) {

                    var Today = new Date();
                    var TimeAttribute = new ActiveXObject('CAPICOM.Attribute');
                    TimeAttribute.Name = CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME;
                    TimeAttribute.Value = Today.getVarDate();
                    Today = null;
                    Signer.AuthenticatedAttributes.Add(TimeAttribute);

                    if(signatureComment)
                    {
                        var CommentAttribute = new ActiveXObject('CAPICOM.Attribute');
                        CommentAttribute.Name = CAPICOM_AUTHENTICATED_ATTRIBUTE_DOCUMENT_DESCRIPTION;
                        CommentAttribute.Value = signatureComment;
                        Signer.AuthenticatedAttributes.Add(CommentAttribute);
                        CommentAttribute = null;
                    }
/*
                    var docName = "My Name is ..";
                    var DocNameAttribute = new ActiveXObject('CAPICOM.Attribute');
                    DocNameAttribute.Name = CAPICOM_AUTHENTICATED_ATTRIBUTE_DOCUMENT_NAME;
                    DocNameAttribute.Value = docName;
                    Signer.AuthenticatedAttributes.Add(DocNameAttribute);
                    DocNameAttribute = null;
/
                } // end with

                //download signed content
                new Ajax.Request(SERVLET_URL, {
					parameters: {'signobjectuuid': uuid, 'mode': this.mode},
					onSuccess: this.download_onSuccess.bind(this, Signer),
					onFailure: function(transport) 
					{
						throw CryptoLocal.SimpleError;
					},
					onException: function(requesterObj, exceptionObj) 
					{
					    ajaxReq.options.onFailure(null);
					},              
					on0: function(transport) 
					{
					    // this gets invoked if the server is down.
					    ajaxReq.options.onFailure(transport);
					},
					asynchronous: false
                });

                //Флаг isError выставляется в функции download_onSuccess(), по-другому признак ошибки из 
                //Ajax.Request не прокинуть сюда:
                if (this.isError)
                {
                	return;
                }
                var item = $(this.container);
                item.stopObserving('click', this.onClickObserver);
                Form.Element.disable(item);

                alert(CryptoLocal.SignedDataComplete);
                if (this.close === true)
                    close_form();
                else
                    if (this.refresh === true)
                        refresh_page();
            }
            else if(validation === false)
            {
                throw CryptoLocal.CertificateNotFound;
            }
            else
            {
                //isValid error
                throw validation;
            }

            //Clean up
            Signer = null;
        }
        catch (e1)
        {
           //TODO: create usability error notification
            var error_description = 'Ошибка';
        	
            if(typeof(e1) == 'string') 
            	error_description = e1;
            else 
        	{
            	if (e1.description)
            		error_description = e1.description;
            	else
            		error_description = e1.message;
        	}
            
            if (error_description.length > 42)
	            if (error_description.substr(0, 42) == 'The requested operation has been cancelled')
	            {
	            	error_description = CryptoLocal.HRESULT_ERROR_CANCELLED;
	            }
            alert(error_description);
        }
    },

    validateCert: function (Certificate)
    {
        if(!Certificate) return false;

        if(!Certificate.KeyUsage().IsDigitalSignatureEnabled)
        {
            return CryptoLocal.DigitalSignIsNotEnabled;
        }

        var cert_rk = Certificate.PrivateKey;
        var cert_uk = Certificate.PublicKey();
        var key_length = cert_uk.Length;
        var csp_name = cert_rk.ProviderName;
        var equil_csp = csp_name == this.providerName;
        var equil_key_length = key_length == this.keyLength;

        if(equil_csp && equil_key_length)
        {
            return checkCertificateStatus(Certificate);
        }

        //see settings in server application
        return CryptoLocal.CspNotAllowed;
    },

    download_onSuccess: function (Signer, transport)
    {
        try
        {
            var SignedData = new ActiveXObject('CAPICOM.SignedData');
            var utils = new ActiveXObject('CAPICOM.Utilities');

            SignedData.Content = utils.Base64Decode(transport.responseText);
            var signature = SignedData.Sign(Signer, CAPICOM.CAPICOM_SIGNATURE_TYPE.DETACHED, CAPICOM.CAPICOM_ENCODING_TYPE.CAPICOM_ENCODE_BASE64);
            var signId = uploadDetachedSignature(signature, this.uuid);

            if (this.useTimeStamp)
            {
                createTimeStamp(signature, this.uuid, this);
            }

            if (this.useAttachedSignature)
            {
                signature = SignedData.Sign(Signer, CAPICOM.CAPICOM_SIGNATURE_TYPE.ATTACHED, CAPICOM.CAPICOM_ENCODING_TYPE.CAPICOM_ENCODE_BASE64);
                uploadAttachedSignature(signature, this.uuid, signId);
            }

            SignedData = null;
        }
        catch (e)
        {
            this.isError = true;
            var error_description = e.description;
            if (error_description.length > 42)
	            if (error_description.substr(0, 42) == 'The requested operation has been cancelled')
	            {
	            	error_description = CryptoLocal.HRESULT_ERROR_CANCELLED;
	            }
            alert(error_description);
        }
    }
});*/

/*function getXEnroll()
{
	try
	{
		return new ActiveXObject('CEnroll.CEnroll.1');
	}
	catch (e) {
		return null;
	}
}*/

/*
 * Methods for support EditCryptoSettingsController
 */
/*function initCryptoProviderList(cspName, algName, keyLengthName)
{
    if (!Prototype.Browser.IE) return;

    var max_ptq = 76;
    var AT_KEYEXCHANGE = 1;
    var AT_SIGNATURE = 2;
     
    var XEnroll = getXEnroll();
    if (XEnroll == null)
    {
    	// if XEnroll == null then this script works on Windows Vista or later
    	initCriptoProviderSettingVista(cspName, algName, keyLengthName);
    	return;
    }    
    
    var cspSelect = document.getElementById(cspName);

    var csp_name = '';
    var csp_num = 0;

    var defaultCsp = document.getElementById(cspName + "Name").value;
    var defaultAlg = document.getElementById(algName + "Id").value;
    var defaultLength = document.getElementById(keyLengthName + "Id").value;

    for (var t = 0; t < max_ptq; t++)
    {
        XEnroll.ProviderType = t;

        for (var p = 0; p < max_ptq; p++)
        {
            try
            {
                csp_name = XEnroll.enumProviders(p, 0);
            }
            catch (e)
            {
                break;
            }

            if (XEnroll.GetSupportedKeySpec() && AT_SIGNATURE)
            {
                cspSelect.options[++csp_num] = new Option(csp_name, t);

                if (csp_name === defaultCsp)
                {
                    cspSelect.options[csp_num].selected = true;
                }
            }
        }
    }

    initAlgClassList(cspName, algName, defaultAlg);
    initKeyLengthList(algName, keyLengthName, defaultLength);

    cspSelect = null;
    XEnroll = null;
}*/

/*
 * Init algoritm select field
 */
/*function  initAlgClassList(cspName, algName, defaultAlg)
{
    if(!Prototype.Browser.IE) return;

    //init crypto providers hidden field
    var cspSelect = document.getElementById(cspName);
    var csp_type = cspSelect.options[cspSelect.selectedIndex].value;
    var csp_name = cspSelect.options[cspSelect.selectedIndex].text;
    document.getElementById(cspName + "Id").value = csp_type;
    document.getElementById(cspName + "Name").value = csp_name;

     CEnroll.EnumAlgs http://msdn.microsoft.com/en-us/library/aa382851(VS.85).aspx 
    var ALG_CLASS_HASH = 4<<13;

    var hashSelect = document.getElementById(algName);

    hashSelect.options.length = 1;

    if (csp_type)
    {
        var max_aq = 15;
        var XEnroll = getXEnroll();
        if (XEnroll == null)
        {
        	// if XEnroll == null then this script works on Windows Vista or later
        	initAlgListVista(makeCSPListObject(), cspName, algName);
        	return;
        }
        
        var alg_id = 0;
        var alg_name = '';
        XEnroll.ProviderType = csp_type;
        XEnroll.ProviderName = csp_name;
        for (var a = 0; a < max_aq; a++)
        {
            try { alg_id = XEnroll.EnumAlgs(a, ALG_CLASS_HASH); } catch (e) { break; }
            alg_name = XEnroll.GetAlgName(alg_id);
            hashSelect.options[a+1] = new Option(alg_name, alg_id);
            if (alg_id == defaultAlg)
            {
                hashSelect.options[a+1].selected = true;
                hashSelect.fireEvent('onchange');
            }
        }

        XEnroll = null;
    }
    else
    {
        hashSelect.fireEvent('onchange');
    }

    cspSelect = null;
    hashSelect = null;
}*/

/*
 *
 */
/*function initKeyLengthList(algName, keyLengthName, defaultLength)
{
    if(!Prototype.Browser.IE) return;

    //init crypto providers hidden field
    var hashSelect = document.getElementById(algName);
    var alg_type = hashSelect.options[hashSelect.selectedIndex].value;

    document.getElementById(algName + "Name").value = hashSelect.options[hashSelect.selectedIndex].text;
    document.getElementById(algName + "Id").value = alg_type;

    //
    var keyLengthSelect = document.getElementById(keyLengthName);

    keyLengthSelect.options.length = 1;

    if (alg_type)
    {
        var KEY_LEN_MIN = true;
        var KEY_LEN_MAX = false;
        var KEY_LEN_MIN_DEFAULT = 384;
        var KEY_LEN_MAX_DEFAULT = 16384;

        var min = KEY_LEN_MIN_DEFAULT;
        var max = KEY_LEN_MAX_DEFAULT;

        var num = 0;

        var XEnroll = getXEnroll();
        if (XEnroll == null)
        {
        	// if XEnroll == null then this script works on Windows Vista or later
        	initKeyLengthListVista(makeCSPListObject(), algName, keyLengthName);
        	return;
        }
        try { min = XEnroll.GetKeyLen(KEY_LEN_MIN, false); } catch (e) {}

        try { max = XEnroll.GetKeyLen(KEY_LEN_MAX, false); } catch (e) {}

        for ( var len = 128; len < min; len *= 2) min = len;

        for ( var len = min; len <= max; len *= 2)
        {
            keyLengthSelect.options[++num] = new Option(len, len);
            if (len == defaultLength)
            {
                keyLengthSelect.options[num].selected = true;
                keyLengthSelect.fireEvent('onchange');
            }
        }
    }
    else
    {
        keyLengthSelect.fireEvent('onchange');
    }

    hashSelect = null;
    keyLengthSelect = null;
}*/

/*
 * Load CSPs list, Algs list and KeyLengthes list to EditCryptoSettings form 
 * (see EditCryptoSettingsController).
 * 
 * Works only Win after XP/2003 (Vista, 7, 2008 Server, ...)
 */
/*function initCriptoProviderSettingVista(cspName, algName, keyLengthName)
{
	if (!Prototype.Browser.IE) 
    	return;
	    
	cspList = makeCSPListObject();

	initCryptoProviderListVista(cspList, cspName);
	initAlgListVista(cspList, cspName, algName);
	initKeyLengthListVista(cspList, algName, keyLengthName);
	
	cspList = null;
}*/

/*
 * Load CSPs list to EditCryptoSettings form (see EditCryptoSettingsController)
 * Works only Win after XP/2003 (Vista, 7, 2008 Server, ...)
 */
/*function initCryptoProviderListVista(cspList, cspName)
{    
    try
    {
    	var cspSelect = $(cspName);
        var defaultCsp = $(cspName + "Name").value;    	    
    	
    	// Convert all available CSPs to Options
    	num = 0
        for (i = 0; i < cspList.Count; i++)
        {
            csp = cspList.ItemByIndex(i);            
            if (csp.KeySpec && CSP_KEY_SPEC.AT_SIGNATURE)
            {            	
            	cspSelect.options[++num] = new Option(csp.Name, csp.Type);
            	if (csp.Name === defaultCsp)
            	{
                	cspSelect.options[num].selected = true;
            	    $(cspName + "Id").value = csp.Type;
            	    $(cspName + "Name").value = csp.Name;
            	}             
            }
        }
    	
        cspSelect = null;
    }
    catch (e) 
    {
    	alert("Произошла ошибка: " + e.description);
	}
}*/

/*
 * Load Algs list to EditCryptoSettings form (see EditCryptoSettingsController)
 * Works only Win after XP/2003 (Vista, 7, 2008 Server, ...)
 */
/*function initAlgListVista(cspList, cspName, algName)
{    
    try
    {
    	var cspSelect = $(cspName);
    	    	
    	// Save selected options to field's values
    	var type = cspSelect.options[cspSelect.selectedIndex].value;    	
        var name = cspSelect.options[cspSelect.selectedIndex].text;
        $(cspName + "Id").value = type;
	    $(cspName + "Name").value = name;
	    
	    var hashSelect = $(algName);
    	    	        
        if (type)
        {        	        	        	                	     
            var defaultAlg = $(algName + "Id").value;
        	
        	csp = cspList.ItemByName(name);
        	if (csp == null)
        	{
        		alert("Не удалось найти криптопровайдер " + name);
        		return;
        	}        	
        	
        	for (i = 0; i < csp.CspAlgorithms.Count; i++)
        	{
        		alg = csp.CspAlgorithms.ItemByIndex(i);         		
        		hashSelect.options[i + 1] = new Option(alg.Name, alg.Type);
                if (alg.Type == defaultAlg)
                {
                    hashSelect.options[a+1].selected = true;
                    hashSelect.fireEvent('onchange');
                }	
        	}
        }
        else {
        	hashSelect.fireEvent('onchange');
        }
    }
    catch (e) 
    {
    	alert("Произошла ошибка: " + e.description);
	}
}*/

/*
 * Load allowed KeyLength list to EditCryptoSettings form (see EditCryptoSettingsController)
 * Works only Win after XP/2003 (Vista, 7, 2008 Server, ...)
 */
/*function initKeyLengthListVista(cspList, algName, keyLengthName)
{    
	// TODO: Не нашел как в CertEnroll получить минимальную и максимальную длину ключа для
	// криптопровайдера - можно только для алгоритма, однако мы выбираем алгоритм хэширования,
	// а не алгоритм шифрования, поэтому параметры ключа для данного алгоритма не подходят.
	// Сделал заглушку - просто добавил числа от 128 до 1024.
    try
    {    	
    	var algSelect = $(algName);
    	
    	// Save selected options to field's values
    	var type = algSelect.options[algSelect.selectedIndex].value;    	
        var name = algSelect.options[algSelect.selectedIndex].text;
        $(algName + "Id").value = type;
	    $(algName + "Name").value = name;
	    
	    var keyLengthSelect = $(keyLengthName);
    	 	    
        if (type)
        {        	        	        	                	     
            var defaultKeyLength = $(keyLengthName + "Id").value;        	            
            
            var num = 0;
            for ( var len = 128; len <= 1024; len *= 2)
            {
            	keyLengthSelect.options[++ num] = new Option(len, len);
            	if (len == defaultKeyLength)
            	{
            		keyLengthSelect.options[num].selected = true;
            		keyLengthSelect.fireEvent('onchange');
            	}
            }
        }
        else 
        {
        	keyLengthSelect.fireEvent('onchange');
        }
    }
    catch (e) 
    {
    	alert("Произошла ошибка: " + e.description);
	}
}

function makeEnrollmentFactory()
{
	try
	{
		return new ActiveXObject("X509Enrollment.CX509EnrollmentWebClassFactory");
	}
	catch (e) {
		alert("Heвозможно создать фабрику объектов работы с сертификатам (X509Enrollment) " +
				"по причине: " + e.description);
	}
}

function makeCSPListObject()
{
	try
	{
		var cspList = makeEnrollmentFactory().CreateObject("X509Enrollment.CCspInformations");
		
		// Load available CSPs to CSPList
    	cspList.AddAvailableCsps();
    	
    	return cspList;
	}
	catch (e) {
		alert("Невозможно создать объект типа CCspInformations" +
				"по причине: " + e.description);
	}
}*/