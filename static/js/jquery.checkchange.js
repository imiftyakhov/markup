(function($) { 

	var defValues = defValues || {};
	function refreshDefValues( formid )
	{
	    defValues = formid ? $.extend( defValues, getFormsDefValues($("#" + formid))) : getFormsDefValues($("form"));
	}
	
	function hasFormsAnyChanges(oForms, prevValues) {
	    var el, opt, prevOptValues, j, id;
	    
	    return new Array( oForms ).some( function( form ) { return checkForm( prevValues, form ); } );	    
	}

	function checkForm( prevValues, form )
	{	
	    if(!form || !form.elements)
	        return false;
	    
	    for (var i=0; el = form.elements[i]; i++) {    	
	    	var id = el.id;
	        
	    	if (id == "" || 
	    		el.form.getAttribute('ignoreChanges') == true || 
	    		el.getAttribute('ignoreChanges') == true ||
	    		prevValues[id]==undefined )
	            continue;
	        	        
			switch (el.type) {
			case 'text' :
			case 'textarea' :
				if( window['tinyMCE'] && tinyMCE.get(id) )
				{
					if( tinyMCE.get(id).getContent().replace(/\s+/g, '+') != prevValues[id].replace(/\s+/g, '+') ) 
					{
						return true;
					}
				}
				else if (el.value != prevValues[id] )
			        return true;
			    break;
			case 'checkbox' :
			case 'radio' :
			    if (el.checked != prevValues[id])
			        return true;
			    break;
			case 'select-one' :
			    if (el.value != prevValues[id])
			        return true;
			    break;
			case 'select-multiple' :
			    prevOptValues = prevValues[id];
			    var arr = new Array(prevOptValues.length);
			    for (j = 0; j < prevOptValues.length; j++){
			        if (el.options[j].selected)
			            arr[j] = el.options[j].value;
			    }
			    arr.sort();
			    prevOptValues.sort();
			    for (j = 0; j < length; j++){
			        if (arr[j] != prevOptValues[j])
			            return true;
			    }
			    break;
			case 'file' :
			    if (el.value != prevValues[id])
			        return true;
			    break;
			}
	    }
	    
		return false;
	}

	function getFormsDefValues(oForms) {
	    var el, opt, prevOptValues, j, id;
	    var defValues = {};
	    for(var k in oForms){
	        if(!oForms[k] || !oForms[k].elements)
	            continue;
	        for (var i=0; el = oForms[k].elements[i]; i++) {
	            if (el.id == "" || el.form.getAttribute('ignoreChanges') == true)
	                continue;
	            
	            id = el.id;

	            if(el.getAttribute('ignoreChanges') == true)
	                continue;

	            switch (el.type) {
	                case 'text' :
	                case 'textarea' :
	                case 'select-one' :
	                    defValues[id]=el.value;
	                    break;
	                case 'checkbox' :
	                case 'radio' :
	                    defValues[id]=el.checked;
	                    break;
	                case 'select-multiple' :
	                    var arr = new Array(el.options.length);
	                    for (j = 0; j < el.options.length; j++){
	                        if(el.options[j].selected)
	                            arr[j] = el.options[j].value;
	                    }
	                    defValues[id]=arr;
	                    break;
	                case 'file' :
	                    defValues[id]="";
	                    break;
	            }
	        }
	    }
	    return defValues;
	}

	$.refreshDefValues = refreshDefValues;
	$.hasFormsAnyChanges = function( oForms ) { return hasFormsAnyChanges( oForms, defValues); }
	$.checkForm = function( form ) { return checkForm( defValues, form ); }
	$.getFormsDefValues = getFormsDefValues;

})(jQuery); 