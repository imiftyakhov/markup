/*  2013/06/21*/
 
(function($) {
	
    $.fn.customCheck = function() {
    	
    	return this.each(function() {
            var $this = $(this),
            	$container = $('<span class="checkbox_container"/>'),
            	$span = $('<span class="checkbox_custom"/>');
            
            if( $this.hasClass("customCheckbox") )
            	return;
                                   
            $this.addClass("customCheckbox");
            $this.is(':checked') && $span.addClass('checked');
            $this.is(':disabled') && $span.addClass('disabled');
            
            $this.wrap( $container );
            $this.closest('.checkbox_container').append( $span );

            $this
            	.closest('label')
	            .addClass('checkbox__label_custom')
	            .attr('onclick', ''); // Fix clicking label in iOS
            
            $this.css({ position: 'absolute',  opacity: '0' });
            
            // Events
            $this.bind({
                change: function() {
                    $span.toggleClass('checked', $this.is(':checked'));
                    $span.toggleClass('disabled', $this.is(':disabled'));
                },
                focus: function() { $span.addClass('focus'); },
                blur: function() { $span.removeClass('focus'); }
            });
            
            $this.hover( function() { $span.addClass('hovered'); }, function() { $span.removeClass('hovered'); } );             
            /* Select all Checkbox */
        });
    };
})(jQuery);
