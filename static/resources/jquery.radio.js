/*  2013/06/21*/
 
(function($) {
	
	$.fn.customRadio = function(){
        return $(this).each(function(){        	
            var p = $(this),
                container = $('<span class="radio__container"/>');
                radio = $('<span class="radio_span"/>');
            var r = p.find('input[type="radio"]');
            r.css({  opacity: '0' });
            
           
            var len = p.find('.radio_span').length;
           
            if (len==0) {
            	p.find('input[type="radio"]').wrap(container);
            	p.find('.radio__container').append(radio);
            }
            p.find('input:checked').parent().addClass('checked');

            var checkFunction = function(){
                p.find('.radio__container').removeClass('checked');
                $(this).parent().addClass('checked');               
            }
            
            // Events
            p.find('input[type="radio"]').bind({
            		'click': checkFunction,
            		focus: function() { $(this).parent().addClass('focus'); },
                    blur: function() { $(this).parent().removeClass('focus'); }
            	})
            	.hover( 
            		function() { 
            			var container = $(this).parent();
            			container.addClass('hovered');
            			container.nextAll('.radio__label').addClass('hovered');
            			
            		}, 
            		function() { 
            			var container = $(this).parent();
            			container.removeClass('hovered');
            			container.nextAll('.radio__label').removeClass('hovered');
            		} 
            	);
                    	
            p.find('.radio__label')
            	.bind('click', checkFunction )
            	.hover( 
            		function() { 
            			var container = $(this).prevAll('.radio__container');
            			container.addClass('hovered');
            			$(this).addClass('hovered');
            		},
            		
            		function() { 
            			var container = $(this).prevAll('.radio__container');
            			container.removeClass('hovered');
            			$(this).removeClass('hovered');
            		} 
            	);
        });
    };
})(jQuery);
