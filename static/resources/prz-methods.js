$.validator.addMethod( "email", function( value, element ) {
	return this.optional( element ) || /^\w{1,}[@][\w\-]{1,}([.]([A-Za-z\-]{1,})){1,3}$/.test( value );
}, "Please enter a valid email address." );

$.validator.addMethod( "login", function( value, element ) {
	return this.optional( element ) || /^[a-zA-Z0-9-._]+$/.test( value );
}, "Please enter a valid email address." );


$.validator.addMethod("passwordConfirm", function( value, element, param ) {
    return $.validator.methods[ "equalTo" ].call( this, value, element, param )
}, "Please enter the same password");

function innValidator(inn)
{
    var innCheckMultiplierSequence = [ 2, 4, 10, 3, 5, 9, 4, 6, 8 ];
    if(!inn)
        return true;

    if (!( (inn.length == 10 || inn.length == 12) && !/[^\d]/.test(inn) ))
        return false;

     if ( inn.length == 12 )
         return true;

     // Механизм валидации ИНН [id=802816]
     var crossProductOfNine = 0;
     for( var i = 0; i < 9; ++i )
     {
         var digit = parseInt( inn.charAt( i ) );
         crossProductOfNine += digit * innCheckMultiplierSequence[i];
     }
     var lastDigit = parseInt(inn.charAt( 9 ) );
     var rem = crossProductOfNine % 11;
     rem = rem % 10;
     return ( lastDigit == rem );
}

$.validator.addMethod( "inn", function( value, element ) {
	return this.optional( element ) || innValidator( value );
}, "Please enter a valid inn." );

$.validator.addMethod( "wicket",  function( value, element, param, method ) {
    // легаси-код из метода remote
    if ( this.optional( element ) ) {
        return "dependency-mismatch";
    }

    // проверяем отличается ли значение проверки от предыдущего
    // получаем последний ответ на запрос
    method = typeof method === "string" && method || "wicket";
    var previous = this.previousValue( element, method )
    // устанавливаем полученное сообщение об ошибке
    if ( !this.settings.messages[ element.name ] ) {
        this.settings.messages[ element.name ] = {};
    }
    this.settings.messages[ element.name ][ method ] = previous.message;
    // формируем текущее значение для проверки
    optionDataString = $.param({ data: value, url: param });
    // если данное значение проверено в предыдущий раз - возвращаем сохраненный результат
    if ( previous.old === optionDataString ) {
        return previous.valid;
    }

    // если в прошлый раз проверялось другое значение - отправляем запрос на бекэнд
    previous.old = optionDataString;
    console.log('wicket ajax validate', value) //todo remote it
    var data = {};
    data[ element.name ] = value;
    var a = Wicket.Ajax.post({"u":param, "wr":true, "ep": data});
    return "pending";

}, "Please fix this field." );

$.validator.wicketback = function(f, el, res, mes){
    var element = $(el)[0] // научите меня jq
    var form = $(f)
    var response = res
    var validator = form.validate();

    // получаем предыдущие значение элемента
    method = typeof method === "string" && method || "wicket";
    var previous = validator.previousValue( element, method )

    // ожидаем что корректный результат проверки - true
    var valid = response === true || response === "true",
        errors, message, submitted;

    if ( valid ) {
        // если проверка успешная - делается какая-то легаси-магия из метода remote
        submitted = validator.formSubmitted;
        validator.resetInternals();
        validator.toHide = validator.errorsFor( element );
        validator.formSubmitted = submitted;
        validator.successList.push( element );
        validator.invalid[ element.name ] = false;
        validator.showErrors();
    } else {
        // если поле невалидно - сохраняем для поля сообщение об ошибке и визуалируем его
        errors = {};
        message = response || mes;
        errors[ element.name ] = message;
        validator.invalid[ element.name ] = true;
        validator.showErrors( errors );
        previous.message = message;
    }
    // сохраняем полученное состояние
    previous.valid = valid;
    // очередная легаси-магия
    validator.stopRequest( element, valid );

};


//$.validator.addMethod( "year", function( value, element ) {
//	return this.optional( element ) || (value > 1900 && value <= new Date().getFullYear());
//}, "Please enter a valid year." );

jQuery.validator.addClassRules("year", {
  min: 1900,
  max: new Date().getFullYear()
});


$.validator.addMethod("length", function( value, element, param ) {
    var length = $.isArray( value ) ? value.length : this.getLength( value, element );
    return this.optional( element ) || length == param;
},  "Please enter at {0} characters.");


$.validator.addMethod("dateMoreThan", function( value, element, param ) {
    if (this.optional( element ))
        return true;
    var ref = (param.charAt[0] === '#') ? $(param).val() : param;
    try {
        return ref.length === 0 ? true : $.datepicker.parseDate('dd.mm.yy', ref) < $.datepicker.parseDate('dd.mm.yy', value)
    }
    catch(err){
        return false;
    }
},  "Please enter a value more than {0}.");

$.validator.addMethod("dateLessThan", function( value, element, param ) {
    if (this.optional( element ))
        return true;
    var ref = (param.charAt[0] === '#') ? $(param).val() : param;
    try {
        return ref.length === 0 ? true : $.datepicker.parseDate('dd.mm.yy', ref) > $.datepicker.parseDate('dd.mm.yy', value)
    }
    catch(err){
        return false;
    }
},  "Please enter a value less than {0}.");

$.validator.addMethod("date", function( value, element, param ) {
    if (this.optional( element ))
        return true;
    try {
        // валидация даты: день от 0 до 31, взависимости от месяца и года, месяц от 1 до 12, год от 1900 до 2100
        var parts = value.split('.');
        var year = parts[2];
        var month = parts[1] - 1;
        var day = parts[0];
        var date = new Date(year, month, day);
        return year == date.getFullYear() && month == date.getMonth() && day == date.getDate() && year > 1900 && year < 2100;
    }
    catch(err){
        return false;
    }
},  "Please enter a valid date.");

$.validator.addMethod("number", function( value, element, param ) {
    return this.optional( element ) || /^(?:-?\d+|-?\d{1,3}(?:\s\d{3})+)?(?:\.\d+|\,\d+)?$/.test( value );
},  "Please enter a valid number.");

/**
 * Function creates module and call for init method
 * @param module
 * @returns {*}
 */
function library(module) {
    $(function () {
        if (module.init) {
            module.init();
        }
    });
    return module;
}


/**
 * Auxiliary namespace for DemandPage jQuery calls
 * @type {*}
 */
var auxiliaryValidators = library(function() {
    return {
        init: function() {},

        getSum: function(number) {
            var koefficients = [ 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1 ];
            var sum = 0;
            for(var i = 0; i < number.length; i++) {
                sum += ((koefficients[i] * Number(number.substring(i, i+1))) % 10);
            }
            return sum;
        },

        checkAccountNumber: function(account, bik) {
            // контрольный разряд
            var key = 0;
            // сумма младших разрядов
            var sum = 0;
            // к номеру лицевого счёта слева (т.е. в начало) добавляют 3 разряда,
            // соответствующие условному номеру кредитной организации, который берется из БИКа банка и соответствует 3-м последним цифрам.
            var accountNumberForValidate = bik.substr(-3) + account;
            // Рассчитываются   произведения   значений   разрядов    на
            // соответствующие весовые коэффициенты.
            // Рассчитывается сумма значений младших разрядов полученных
            // произведений.
            key = this.getSum(accountNumberForValidate);
            // Младший разряд вычисленной суммы умножается на 3.
            // Значение контрольного   ключа  принимается  равным  младшему
            // разряду полученного произведения.
            key = ((key % 10) * 3) % 10;
            // Подставляем вычисленнный контрольный разряд,
            accountNumberForValidate[11] = String(key);
            // Рассчитываются   произведения   значений   разрядов    на
            // соответствующие весовые коэффициенты с учетом контрольного ключа.
            // Рассчитывается   сумма   младших   разрядов    полученных
            // произведений.
            sum = this.getSum(accountNumberForValidate);
            // При получении  суммы,  кратной  10  (младший  разряд  равен   0),
            // значение контрольного ключа считается верным.
            return (sum % 10 === 0);
        },

        okpoValidator: function(okpo) {
            var okpoLength = okpo.length;
            if (okpoLength != 8 && okpoLength != 10) {
                return false;
            }
            // Контрольной цифрой кода является последняя цифра -
            // восьмая в восьмизначном коде и десятая в десятизначном
            var controlCodeNumber = Number(okpo.substr(-1));
            // Разрядам кода в общероссийском классификаторе, начиная со старшего разряда,
            // присваивается набор весов, соответствующий натуральному ряду чисел от 1 до 10.
            // Если разрядность кода больше 10, то набор весов повторяется
            // Каждая цифра кода, кроме последней, умножается на вес разряда
            // и вычисляется сумма полученных произведений.
            var sum = 0;
            for (var i = 0; i < okpoLength - 1; i++) {
                sum += Number(okpo.substring(i, i + 1)) * ((i + 1) % 11);
            }
            // Контрольное число для кода представляет собой остаток от деления полученной суммы на модуль «11».
            var controlNumber = sum % 11;
            // Контрольное число должно иметь один разряд, значение которого находится в пределах от 0 до 9
            if (controlNumber != 10) {
                return controlNumber === controlCodeNumber;
            } else {
                // Если получается остаток, равный 10,
                // то для обеспечения одноразрядного контрольного числа необходимо провести повторный расчет,
                // применяя вторую последовательность весов, сдвинутую на два разряда влево
                sum = 0;
                for (var i = 0; i < okpoLength - 1; i++) {
                    sum += Number(okpo.substring(i, i + 1)) * ((i + 3) % 11);
                }
                controlNumber = sum % 11;
                // Если в случае повторного расчета остаток от деления вновь сохраняется равным 10,
                // то значение контрольного числа проставляется равным «0».
                if (controlNumber === 10) {
                    return 0 === controlCodeNumber;
                } else {
                    return controlNumber === controlCodeNumber;
                }
            }
        },

        ipAddressRangeValidator: function(addresses) {
            var numberAddresses = addresses.length;
            var ipv4Pattern = /^(25[0-5]|2[0-4]\d|[0-1]?\d?\d)(\.(25[0-5]|2[0-4]\d|[0-1]?\d?\d)){3}$/;
            var ipv6StdPattern = /^(?:[0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}$/;
            var ipv6HexPattern = /^((?:[0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4})*)?)::((?:[0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4})*)?)$/;

            for (var i = 0; i < numberAddresses; i++) {
                if (!(ipv4Pattern.test(addresses[i]) || ipv6StdPattern.test(addresses[i]) || ipv6HexPattern.test(addresses[i]))) {
                    return false;
                }
            }
            return true;
        }
    };
}());

$.validator.addMethod("accountNumber",
    function (value, element, param)
    {
        if (this.optional(element)) {
            true;
        }
        var ref = (param.charAt[0] === '#') ? $(param).val() : param;

        try {
            return auxiliaryValidators.checkAccountNumber(value, ref);
        }
        catch (err) {
            return false;
        }
    },
    "Please enter a valid account number"
);

$.validator.addMethod("okpo",
    function (value, element, param) {
        return this.optional(element) || auxiliaryValidators.okpoValidator(value);
    },
    "Please enter a valid OKPO number"
);

$.validator.addMethod("ipAddressRange",
    function (value, element, param) {
        var addresses = value.replace(/[ ]/g, "").replace(/[-]/g, ",").split(',');

        return this.optional(element) || auxiliaryValidators.ipAddressRangeValidator(addresses);
    },
    "Please enter a valid ip address range"
);
